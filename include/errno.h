#ifndef SOLC_ERRNO_H
#define SOLC_ERRNO_H

#include <sys/cdefs.h>

/*
 * TODO: Possibly start working on Annex K.
 */

/*
 * 7.5 Errors <errno.h>
 * 1. The header <errno.h> defines several macros, all relating to the reporting of error conditions.
 * 2. The macros are
 *    - EDOM
 *    - EILSEQ
 *    - ERANGE
 *    which expand to integer constant expressions with type int, distinct positive values, and which
 *    are suitable for use in #if preprocessing directives; and
 *    - errno
 *    which expands to a modifiable lvalue that has type int and thread local storage duration, the
 *    value of which is set to a positive error number by several library functions. If a macro
 *    definition is suppressed in order to access an actual object, or a program defines an
 *    identifier with the name errno, the behavior is undefined.
 * 3. The value of errno in the initial thread is zero at program startup (the initial value of
 *    errno in other threads in an indeterminate value), but is never set to zero by any library
 *    function. The value of errno may be set to nonzero by a library function call whether or not
 *    there is an error, provided the use of errno is not documented in the description of the
 *    function in this document.
 * 4. Additional macro definitions, beginning with E and a digit or E and an uppercase letter,
 *    may also be specified by the implementation.
 */

__BEGIN_DECLS

#define EDOM   33  /* Mathematics argument out of domain of function. */
#define EILSEQ 138 /* Illegal byte sequence. */
#define ERANGE 34  /* Result too large */

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)
#    ifdef __STDC_NO_THREADS__ /* Unfortunately I have to manually sync it with the threads.h */
extern int errno;
#    else
extern _Thread_local int errno;
#    endif /* __STDC_NO_THREADS__ */
#else
extern int errno;
#endif

int* __errno_location(void);
#define errno           (*__errno_location())

#define ESUCCESS        0      /* Success (not an error) */
#define EPERM           1      /* Operation not permitted. */
#define ENOENT          2      /* No such file or directory. */
#define ESRCH           3      /* No such process. */
#define EINTR           4      /* Interrupted function. */
#define EIO             5      /* I/O error. */
#define ENXIO           6      /* No such device or address. */
#define E2BIG           7      /* Argument list too long. */
#define ENOEXEC         8      /* Executable file format error. */
#define EBADF           9      /* Bad file descriptor. */
#define ECHILD          10     /* No child processes. */
#define EAGAIN          11     /* Resource unavailable, try again. */
#define EWOULDBLOCK     EAGAIN /* Operation would block. */
#define ENOMEM          12     /* Not enough space. */
#define EACCES          13     /* Permission denied. */
#define EFAULT          14     /* Bad address. */
#define EBUSY           16     /* Device or resource busy. */
#define EEXIST          17     /* File exists. */
#define EXDEV           18     /* Cross-device link. */
#define ENODEV          19     /* No such device. */
#define ENOTDIR         20     /* Not a directory or a symbolic link to a directory. */
#define EISDIR          21     /* Is a directory. */
#define EINVAL          22     /* Invalid argument. */
#define ENFILE          23     /* Too many files open in system. */
#define EMFILE          24     /* File descriptor value too large. */
#define ENOTTY          25     /* Inappropriate I/O control operation. */
#define ETXTBSY         26     /* Text file busy. */
#define EFBIG           27     /* File too large. */
#define ENOSPC          28     /* No space left on device. */
#define ESPIPE          29     /* Invalid seek. */
#define EROFS           30     /* Read-only file system. */
#define EMLINK          31     /* Too many links. */
#define EPIPE           32     /* Broken pipe. */
#define ENOMSG          35     /* No message of the desired type. */
#define EIDRM           36     /* Identifier removed. */
#define EDEADLK         45     /* Resource deadlock would occur. */
#define ENOLCK          46     /* No locks available. */
#define ENOSTR          60     /* Not a STREAM. */
#define ENODATA         61     /* No message is available on the STREAM head read queue. */
#define ETIME           62     /* Stream ioctl() timeout */
#define ENOSR           63     /* No STREAM resources. */
#define ENOLINK         67     /* Reserved. */
#define EPROTO          71     /* Protocol error. */
#define EMULTIHOP       74     /* Reserved. */
#define EBADMSG         77     /* Bad message. */
#define ENOSYS          88     /* Functionality not supported. */
#define ENOTEMPTY       90     /* Directory not empty. */
#define ENAMETOOLONG    91     /* Filename too long. */
#define ELOOP           92     /* Too many levels of symbolic links. */
#define EOPNOTSUPP      95     /* Operation not supported on socket */
#define ECONNRESET      104    /* Connection reset. */
#define ENOBUFS         105    /* No buffer space available. */
#define EAFNOSUPPORT    106    /* Address family not supported. */
#define EPROTOTYPE      107    /* Protocol wrong type for socket. */
#define ENOTSOCK        108    /* Not a socket. */
#define ENOPROTOOPT     109    /* Protocol not available. */
#define ECONNREFUSED    111    /* Connection refused. */
#define EADDRINUSE      112    /* Address in use. */
#define ECONNABORTED    113    /* Connection aborted. */
#define ENETUNREACH     114    /* Network unreachable. */
#define ENETDOWN        115    /* Network is down. */
#define ETIMEDOUT       116    /* Connection timed out. */
#define EHOSTUNREACH    118    /* Host is unreachable. */
#define EINPROGRESS     119    /* Operation in progress. */
#define EALREADY        120    /* Connection already in progress. */
#define EDESTADDRREQ    121    /* Destination address required. */
#define EMSGSIZE        122    /* Message too large. */
#define EPROTONOSUPPORT 123    /* Protocol not supported. */
#define EADDRNOTAVAIL   125    /* Address not available. */
#define ENETRESET       126    /* Connection aborted by network. */
#define EISCONN         127    /* Socket is connected. */
#define ENOTCONN        128    /* The socket is not connected. */
#define EDQUOT          132    /* Reserved. */
#define ESTALE          133    /* Reserved. */
#define ENOTSUP         134    /* Not supported */
#define EOVERFLOW       139    /* Value too large to be stored in data type. */
#define ECANCELED       140    /* Operation canceled. */
#define ENOTRECOVERABLE 141    /* State not recoverable. */
#define EOWNERDEAD      142    /* Previous owner died. */
#define EMAXERRNO       143    /* The highest errno + 1, no need to worry. */

__END_DECLS

#endif /* SOLC_ERRNO_H */
