#ifndef SOLC_TGMATH_H
#define SOLC_TGMATH_H

#include <complex.h>
#include <math.h>
/* TODO: Add support for C99 */
#define __STDC_VERSION_TGMATH_H__ 202111L /* The same thing with the other macros */

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201112L)

#    define __FUNCTION_SELECTION(FUNC, TYPE, PREFIX, SUFFIX) , TYPE : PREFIX##FUNC##SUFFIX
#    if defined(__STDC_IEC_60559_DFP__) && defined(_Imaginary_I)
#        define __ENUMERATE_ALL_TYPES(V, F)   \
            V(F, double, , )                  \
            V(F, float, , f)                  \
            V(F, long double, , l)            \
            V(F, _Decimal32, , d32)           \
            V(F, _Decimal64, , d64)           \
            V(F, _Decimal128, , d128)         \
            V(F, double complex, c, )         \
            V(F, float complex, c, f)         \
            V(F, long double complex, c, l)   \
            V(F, double imaginary, c, )       \
            V(F, float imaginary, c, f)       \
            V(F, long double imaginary, c, l) \
            V(F, default, , )
#    elif defined(__STDC_IEC_60559_DFP__) && !defined(_Imaginary_I)
#        define __ENUMERATE_ALL_TYPES(V, F) \
            V(F, double, , )                \
            V(F, float, , f)                \
            V(F, long double, , l)          \
            V(F, _Decimal32, , d32)         \
            V(F, _Decimal64, , d64)         \
            V(F, _Decimal128, , d128)       \
            V(F, double complex, c, )       \
            V(F, float complex, c, f)       \
            V(F, long double complex, c, l) \
            V(F, default, , )
#    elif !defined(__STDC_IEC60559_DFP__) && defined(_Imaginary_I) && 0 /* TODO: Wait for support to land for this */
#        define __ENUMERATE_ALL_TYPES(V, F)   \
            V(F, double, , )                  \
            V(F, float, , f)                  \
            V(F, long double, , l)            \
            V(F, double complex, c, )         \
            V(F, float complex, c, f)         \
            V(F, long double complex, c, l)   \
            V(F, double imaginary, c, )       \
            V(F, float imaginary, c, f)       \
            V(F, long double imaginary, c, l) \
            V(F, default, , )
#    else
#        define __ENUMERATE_ALL_TYPES(V, F) \
            V(F, double, , )                \
            V(F, float, , f)                \
            V(F, long double, , l)          \
            V(F, double complex, c, )       \
            V(F, float complex, c, f)       \
            V(F, long double complex, c, l) \
            V(F, default, , )
#    endif /* Decimal and imaginary types */
#    if defined(__STDC_IEC60559_DFP__)
#        define __ENUMERATE_REAL_TYPES(V, F) \
            V(F, double, , )                 \
            V(F, float, , f)                 \
            V(F, long double, , l)           \
            V(F, _Decimal32, , d32)          \
            V(F, _Decimal64, , d64)          \
            V(F, _Decimal128, , d128)        \
            V(F, default, , )
#    else
#        define __ENUMERATE_REAL_TYPES(V, F) \
            V(F, double, , )                 \
            V(F, float, , f)                 \
            V(F, long double, , l)           \
            V(F, default, , )
#    endif /* Real types */
#    if defined(__STDC_IEC60559_DFP__)
#        define __ENUMERATE_ONLY_REAL_TYPES(V, F) \
            V(F, double, , )                      \
            V(F, float, , f)                      \
            V(F, long double, , l)                \
            V(F, _Decimal32, , d32)               \
            V(F, _Decimal64, , d64)               \
            V(F, _Decimal128, , d128)
#    else
#        define __ENUMERATE_ONLY_REAL_TYPES(V, F) \
            V(F, double, , )                      \
            V(F, float, , f)                      \
            V(F, long double, , l)
#    endif /* Only real types */
#    if defined(__STDC_IEC_60559_DFP__)
#        define __ENUMERATE_DECIMAL_TYPES(V, F) \
            V(F, _Decimal32, , d32)             \
            V(F, _Decimal64, , d64)             \
            V(F, _Decimal128, , d128)           \
            V(F, default, , d64)
#    endif /* Decimal types */
#    define __ENUMERATE_C_REAL_TYPES(V, F) \
        V(F, float, , f)                   \
        V(F, double, , )                   \
        V(F, long double, , l)             \
        V(F, default, , )
#    if defined(_Imaginary_I) && 0 /* TODO: Wait for support to land for this */
#        define __ENUMERATE_COMPLEX_TYPES(V, F) \
            V(F, float complex, c, f)           \
            V(F, double complex, c, )           \
            V(F, long double complex, c, l)     \
            V(F, float imaginary, c, f)         \
            V(F, double imaginary, c, )         \
            V(F, long double imaginary, c, l)   \
            V(F, default, c, )
#    else
#        define __ENUMERATE_COMPLEX_TYPES(V, F) \
            V(F, float complex, c, f)           \
            V(F, double complex, c, )           \
            V(F, long double complex, c, l)     \
            V(F, default, c, )
#    endif /* Imaginary types */
#    if defined(_Imaginary_I) && 0 /* TODO: Wait for support to land for this */
#        define __ENUMERATE_ONLY_COMPLEX_TYPES(V, F) \
            V(F, float complex, c, f)           \
            V(F, double complex, c, )           \
            V(F, long double complex, c, l)     \
            V(F, float imaginary, c, f)         \
            V(F, double imaginary, c, )         \
            V(F, long double imaginary, c, l)
#    else
#        define __ENUMERATE_ONLY_COMPLEX_TYPES(V, F) \
            V(F, float complex, c, f)           \
            V(F, double complex, c, )           \
            V(F, long double complex, c, l)
#    endif /* Only complex types */
#    define __ENUMERATE_C_COMPLEX_TYPES(V, F) \
        V(F, float complex, c, f)             \
        V(F, double complex, c, )             \
        V(F, long double complex, c, l)       \
        V(F, default, c, )
#else

#endif /* std > C11 */

/* clang-format off */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
#define acos(x)                                           \
    _Generic((x)                                          \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, acos) \
    )(x)

#define asin(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, asin)  \
    )(x)

#define atan(x)                                           \
    _Generic((x)                                          \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, atan) \
    )(x)

#define acosh(x)                                           \
    _Generic((x)                                           \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, acosh) \
    )(x)

#define asinh(x)                                           \
    _Generic((x)                                           \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, asinh) \
    )(x)

#define atanh(x)                                           \
    _Generic((x)                                           \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, atanh) \
    )(x)

#define cos(x)                                           \
    _Generic((x)                                         \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, cos) \
    )(x)

#define sin(x)                                           \
    _Generic((x)                                         \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, sin) \
    )(x)

#define tan(x)                                           \
    _Generic((x)                                         \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, tan) \
    )(x)

#define cosh(x)                                           \
    _Generic((x)                                          \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, cosh) \
    )(x)

#define sinh(x)                                           \
    _Generic((x)                                          \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, sinh) \
    )(x)

#define tanh(x)                                           \
    _Generic((x)                                          \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, tanh) \
    )(x)

#define exp(x)                                           \
    _Generic((x)                                         \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, exp) \
    )(x)

#define log(x)                                           \
    _Generic((x)                                         \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, log) \
    )(x)

#define pow(x, a)                                        \
    _Generic(((x) + (a))                                 \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, pow) \
    )(x, a)

#define sqrt(x)                                           \
    _Generic((x)                                          \
        __ENUMERATE_ALL_TYPES(__FUNCTION_SELECTION, sqrt) \
    )(x)

#define fabs(x)                                                   \
    _Generic((x)                                                  \
        __ENUMERATE_ONLY_REAL_TYPES(__FUNCTION_SELECTION, fabs)   \
        __ENUMERATE_ONLY_COMPLEX_TYPES(__FUNCTION_SELECTION, abs) \
        , default: fabs                                           \
    )(x)

#define acospi(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, acospi) \
    )(x)

#define asinpi(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, asinpi) \
    )(x)

#define atan2pi(y, x)                                         \
    _Generic(((y) + (x))                                      \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, atan2pi) \
    )(y, x)

#define atan2(y, x)                                         \
    _Generic(((y) + (x))                                    \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, atan2) \
    )(y, x)

#define atanpi(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, atanpi) \
    )(x)

#define cbrt(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, cbrt) \
    )(x)

#define ceil(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, ceil) \
    )(x)

#define compoundn(x, n)                                         \
    _Generic((x)                                                \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, compoundn) \
    )(x, n)

#define copysign(x, y)                                         \
    _Generic(((x) + (y))                                       \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, copysign) \
)   (x, y)

#define cospi(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, cospi) \
    )(x)

#define erfc(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, erfc) \
    )(x)

#define erf(x)                                            \
    _Generic((x)                                          \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, erf) \
    )(x)

#define exp10m1(x)                                            \
    _Generic((x)                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, exp10m1) \
    )(x)

#define exp10(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, exp10) \
    )(x)

#define exp2m1(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, exp2m1) \
    )(x)

#define exp2(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, exp2) \
    )(x)

#define expm1(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, expm1) \
    )(x)

#define fdim(x,y)                                          \
    _Generic(((x) - (y))                                   \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fdim) \
    )(x, y)

#define floor(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, floor) \
    )(x)

#define fmax(x, y)                                         \
    _Generic(((x) + (y))                                   \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmax) \
    )(x, y)

#define fmaximum(x, y)                                         \
    _Generic(((x) + (y))                                       \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmaximum) \
    )(x, y)

#define fmaximum_mag(x, y)                                         \
    _Generic(((x) + (y))                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmaximum_mag) \
    )(x, y)

#define fmaximum_num(x, y)                                         \
    _Generic(((x) + (y))                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmaximum_num) \
    )(x, y)

#define fmaximum_mag_num(x, y)                                         \
    _Generic(((x) + (y))                                               \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmaximum_mag_num) \
    )(x, y)

#define fma(x, y, z)                                      \
    _Generic((((x) * (y)) + (z))                          \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fma) \
    )(x, y, z)

#define fmin(x, y)                                         \
    _Generic(((x) + (y))                                   \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmin) \
    )(x, y)

#define fminimum(x, y)                                         \
    _Generic(((x) + (y))                                       \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fminimum) \
    )(x, y)

#define fminimum_mag(x, y)                                         \
    _Generic(((x) + (y))                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fminimum_mag) \
    )(x, y)

#define fminimum_num(x, y)                                         \
    _Generic(((x) + (y))                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fminimum_num) \
    )(x, y)

#define fminimum_mag_num(x, y)                                        \
    _Generic(((x) + (y))                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fminimum_mag_num)\
    )(x, y)

#define fmod(x, y)                                         \
    _Generic(((x) / (y))                                   \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fmod) \
    )(x, y)

#define frexp(x, p)                                         \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, frexp) \
    )(x, p)

#define fromfpx(x, r, w)                                      \
    _Generic((x)                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fromfpx) \
    )(x, r, w)

#define fromfp(x, r, w)                                      \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, fromfp) \
    )(x, r, w)

#define hypot(x, y)                                         \
    _Generic(((x) + (y))                                    \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, hypot) \
    )(x, y)

#define ilogb(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, ilogb) \
    )(x)

#define ldexp(x, p)                                         \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, ldexp) \
    )(x, p)

#define lgamma(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, lgamma) \
    )(x)

#define llogb(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, llogb) \
    )(x)

#define llrint(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, llrint) \
    )(x)

#define llround(x)                                            \
    _Generic((x)                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, llround) \
    )(x)

#define log10p1(x)                                            \
    _Generic((x)                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, log10p1) \
    )(x)

#define log10(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, log10) \
    )(x)

#define log1p(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, log1p) \
    )(x)

#define log2p1(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, log2p1) \
    )(x)

#define log2(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, log2) \
    )(x)

#define logb(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, logb) \
    )(x)

#define logp1(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, logp1) \
    )(x)

#define lrint(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, lrint) \
    )(x)

#define lround(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, lround) \
    )(x)

#define nearbyint(x)                                            \
    _Generic((x)                                                \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, nearbyint) \
    )(x)

#define nextafter(x, y)                                         \
    _Generic(((y) - (x))                                        \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, nextafter) \
    )(x, y)

#define nextdown(x)                                            \
    _Generic((x)                                               \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, nextdown) \
    )(x)

#define nexttoward(x, y)                                         \
    _Generic((x)                                                 \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, nexttoward) \
    )(x, y)

#define nextup(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, nextup) \
    )(x)

#define pown(x, n)                                         \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, pown) \
    )(x, n)

#define powr(y, x)                                         \
    _Generic(((x) * (y))                                   \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, powr) \
    )(y, x)

#define remainder(x, y)                                         \
    _Generic(((x) / (y))                                        \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, remainder) \
    )(x, y)

#define remquo(x, y, q)                                      \
    _Generic(((x) / (y))                                     \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, remquo) \
    )(x, y, q)

#define rint(x)                                            \
    _Generic((x)                                           \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, rint) \
    )(x)

#define rootn(x, n)                                         \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, rootn) \
    )(x, n)

#define roundeven(x)                                            \
    _Generic((x)                                                \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, roundeven) \
    )(x)

#define round(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, round) \
    )(x)

#define rsqrt(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, rsqrt) \
    )(x)

#define scalbln(x, n)                                         \
    _Generic((x)                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, scalbln) \
    )(x, n)

#define scalbn(x, n)                                         \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, scalbn) \
    )(x, n)

#define sinpi(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, sinpi) \
    )(x)

#define tanpi(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, tanpi) \
    )(x)

#define tgamma(x)                                            \
    _Generic((x)                                             \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, tgamma) \
    )(x)

#define trunc(x)                                            \
    _Generic((x)                                            \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, trunc) \
    )(x)

#define ufromfpx(x, r, w)                                      \
    _Generic((x)                                               \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, ufromfpx) \
    )(x, r, w)

#define ufromfp(x, r, w)                                      \
    _Generic((x)                                              \
        __ENUMERATE_REAL_TYPES(__FUNCTION_SELECTION, ufromfp) \
    )(x, r, w)
/* TODO: Decide whether the enumeration must change */
#define carg(z)                                              \
    _Generic((z)                                             \
        __ENUMERATE_COMPLEX_TYPES(__FUNCTION_SELECTION, arg) \
    )(z)

#define cimag(z)                                              \
    _Generic((z)                                              \
        __ENUMERATE_COMPLEX_TYPES(__FUNCTION_SELECTION, imag) \
    )(z)

#define conj(z)                                              \
    _Generic((z)                                             \
        __ENUMERATE_COMPLEX_TYPES(__FUNCTION_SELECTION, onj) \
    )(z)

#define cproj(z)                                              \
    _Generic((z)                                              \
        __ENUMERATE_COMPLEX_TYPES(__FUNCTION_SELECTION, proj) \
    )(z)

#define creal(z)                                              \
    _Generic((z)                                              \
        __ENUMERATE_COMPLEX_TYPES(__FUNCTION_SELECTION, real) \
    )(z)

#define fadd(x, y)         \
    _Generic(((x) + (y)),  \
        double: fadd,      \
        long double: faddl,\
        default: fadd      \
    )(x, y)

#define dadd(x, y) daddl(x, y)

#define fsub(x, y)         \
    _Generic(((x) - (y)),  \
        double: fsub,      \
        long double: fsubl,\
        default: fsub      \
    )(x, y)

#define dsub(x, y) dsubl(x, y)

#define fmul(x, y)         \
    _Generic(((x) * (y)),  \
        double: fmul,      \
        long double: fmull,\
        default: fmul      \
    )(x, y)

#define dmul(x, y) dmull(x, y)

#define fdiv(x, y)         \
    _Generic(((x) / (y)),  \
        double: fdiv,      \
        long double: fdivl,\
        default: fdiv      \
    )(x, y)

#define ddiv(x, y) ddivl(x, y)

#define ffma(x, y, z)             \
    _Generic((((x) * (y)) + (z)), \
        double: ffma,             \
        long double: ffmal,       \
        default: ffma             \
    )(x, y, z)

#define dfma(x, y, z) dfmal(x, y, z)

#define fsqrt(x)             \
    _Generic((x),            \
        double: fsqrt,       \
        long double: fsqrtl, \
        default: fsqrt       \
    )(x)

#define dsqrt(x) dsqrtl(x)

#   if defined(__STDC_IEC_60559_DFP__)
#define d32add(x, y)             \
    _Generic((((x) + (y))),      \
        _Decimal64: d32addd64,   \
        _Decimal128: d32addd128, \
        default: d32addd64       \
    )(x, y)

#define d64add(x, y) d64addd128(x, y)

#define d32sub(x, y)             \
    _Generic((((x) - (y))),      \
        _Decimal64: d32subd64,   \
        _Decimal128: d32subd128, \
        default: d32subd64       \
    )(x, y)

#define d64sub(x, y) d64subd128(x, y)

#define d32mul(x, y)             \
    _Generic((((x) * (y))),      \
        _Decimal64: d32muld64,   \
        _Decimal128: d32muld128, \
        default: d32muld64       \
    )(x, y)

#define d64mul(x, y) d64muld128(x, y)

#define d32div(x, y)             \
    _Generic((((x) / (y))),      \
        _Decimal64: d32divd64,   \
        _Decimal128: d32divd128, \
        default: d32divd64       \
    )(x, y)

#define d64div(x, y) d64divd128(x, y)

#define d32fma(x, y, z)           \
    _Generic((((x) * (y)) + (z)), \
        _Decimal64: d32fmad64,    \
        _Decimal128: d32fmad128,  \
        default: d32fmad64        \
    )(x, y, z)

#define d64fma(x, y, z) d64fmad128(x, y, z)

#define d32sqrt(x)                  \
    _Generic((x),                   \
        _Decimal64: d32sqrtd64,     \
        _Decimal128: d32sqrtd128,   \
        default: d32sqrtd64         \
    )(x)

#define d64sqrt(x) d64sqrtd128(x)

#define quantize(x, y)                                            \
    _Generic(((x) + (y))                                          \
        __ENUMERATE_DECIMAL_TYPES(__FUNCTION_SELECTION, quantize) \
    )(x, y)

#define samequantum(x, y)                                            \
    _Generic(((x) + (y))                                             \
        __ENUMERATE_DECIMAL_TYPES(__FUNCTION_SELECTION, samequantum) \
    )(x, y)

#define quantum(x)                                               \
    _Generic((x)                                                 \
        __ENUMERATE_DECIMAL_TYPES(__FUNCTION_SELECTION, quantum) \
    )(x)

#define llquantexp(x)                                               \
    _Generic((x)                                                    \
        __ENUMERATE_DECIMAL_TYPES(__FUNCTION_SELECTION, llquantexp) \
    )(x)
#   endif
#endif
/* clang-format on */
#endif /* SOLC_TGMATH_H */
