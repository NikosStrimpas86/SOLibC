#ifndef SOLC_STDINT_H
#define SOLC_STDINT_H

/*
 * 7.20 Integer types <stdint.h>
 *
 * 1. The header <stdint.h> declares sets of integer types having specified widths,
 *    and defines corresponding sets of macros. It also defines macros that
 *    specify limits of integer types corresponding to types defined in other
 *    standard headers.
 * 2. Types are defined in the following categories
 *      - integer types having certain exact widths;
 *      - integer types having at least certain specified widths
 *      - fastest integer types having at least certain specified widths;
 *      - integer types wide enough to hold pointers to objects;
 *      - integer types having greatest width
 *     (Some of these types may denote the same type)
 * 3. Corresponding macros specify limits of the declared types and construct suitable constants.
 * 4. For each type described herein that the implementation provides, <stdint.h> shall
 *    declare that typedef name and define the associated macros. Conversely,
 *    for each type described herein that the implementation does not provide, <stdint.h> shall
 *    not declate that typedef name nor shall it define the associated macros. And implementation
 *    shall provide those types described as "required" but need not provide any of the others
 *    (described as "optional").
 * 5. The feature test macro __STDC_VERSION_STDINT_H__ expands to the token yyyymmL.
 *
 */

#define __STDC_VERSION_STDINT_H__ 202111L

/*
 * 7.20.1 Integer types
 *
 * 1. When typedef names differing only in the absence or presence of the initial u are defined,
 *    they shall denote corresponding signed and unsigned types as described in 6.2.5;
 *    An implementation providing one of these corresponding types shall also provide the other.
 * 2. In the following descriptions, the symbol N represents an unsigned decimal integer with no
 *    leading zeros (e.g., 8 or 24, but nor 04 or 048).
 *
 */

/*
 * 7.20.1.1 Exact width integer types
 *
 * 1. The typedef name intN_t designates a signed integer type with width N and no padding bits.
 *    Thus, int8_t denotes such a signed integer type with a width of exactly 8 bits.
 * 2. The typedef name uintN_t designates an unsigned integerr type with width N and no padding bits.
 *    Thus, uint24_t denotes such an unsigned integer type with a width of exactly 24 bits.
 * 3. These types are optional. However, if an implementation provides integer types with widths
 *    of 8, 16, 32, or 64 bits, and no padding bits, it shall define the corresponding typedef names.
 */

typedef __INT8_TYPE__ int8_t;
typedef __INT16_TYPE__ int16_t;
typedef __INT32_TYPE__ int32_t;
typedef __INT64_TYPE__ int64_t;

typedef __UINT8_TYPE__ uint8_t;
typedef __UINT16_TYPE__ uint16_t;
typedef __UINT32_TYPE__ uint32_t;
typedef __UINT64_TYPE__ uint64_t;

/*
 * 7.20.1.2 Minimum-width integer types
 *
 * 1. The typedef name int_leastN_t designates a signed integer type with a width of at least N, such
 *    that no signed integer type with lesser size has at least the specified width. Thus, int_least32_t
 *    denotes a signed integer type with a width of at least 32 bits.
 * 2. The typedef name uint_leastN_t designates an unsigned integer type with a width of at
 *    least N, such that no unsigned integer type with lesser size has at least the specified width.
 *    Thus, uint_least16_t denotes an unsigned integer type with a width of at least 16 bits.
 * 3. If the typedef name intN_t is defined, int_leastN_t designates the same type. If the typedef
 *    name uintN_t is defined, uint_leastN_t designates the same type.
 * 4. The following types are required:
 *      int_least8_t        uint_least8_t
 *      int_least16_t       uint_least16_t
 *      int_least32_t       uint_least32_t
 *      int_least64_t       uint_least64_t
 *    All other types of this form are optional
 */

typedef __INT_LEAST8_TYPE__ int_least8_t;
typedef __INT_LEAST16_TYPE__ int_least16_t;
typedef __INT_LEAST32_TYPE__ int_least32_t;
typedef __INT_LEAST64_TYPE__ int_least64_t;

typedef __UINT_LEAST8_TYPE__ uint_least8_t;
typedef __UINT_LEAST16_TYPE__ uint_least16_t;
typedef __UINT_LEAST32_TYPE__ uint_least32_t;
typedef __UINT_LEAST64_TYPE__ uint_least64_t;

/*
 * 7.20.1.3 Fastest minimum-width integer types
 *
 * 1. Each of the following types designates an integer type that is usually fastest to operate with
 *    among all integer types that have at least the specified width.
 * 2. The typedef name int_fastN_t designates the fastest signed integer type with a width of at least N.
 *    The typedef name uint_fastN_t designates the fastest unsigned integer type with a width of at
 *    least N.
 * 3. The following types are required:
 *      int_fast8_t         uint_fast8_t
 *      int_fast16_t        uint_fast16_t
 *      int_fast32_t        uint_fast32_t
 *      int_fast64_t        uint_fast64_t
 *    All other types of this form are optional
 */

typedef __INT_FAST8_TYPE__ int_fast8_t;
typedef __INT_FAST16_TYPE__ int_fast16_t;
typedef __INT_FAST32_TYPE__ int_fast32_t;
typedef __INT_FAST64_TYPE__ int_fast64_t;

typedef __UINT_FAST8_TYPE__ uint_fast8_t;
typedef __UINT_FAST16_TYPE__ uint_fast16_t;
typedef __UINT_FAST32_TYPE__ uint_fast32_t;
typedef __UINT_FAST64_TYPE__ uint_fast64_t;

/*
 * 7.20.1.4 Integer types capable of holding object pointers
 *
 * 1. The following type designates a signed integer type with the property that any valid pointer
 *    to void can be converted to this type, then converted back to pointer to void, and the result
 *    will compare equal to the original pointer:
 * -    intptr_t
 *    The following type designates an unsigned integer type with the property that any valid pointer
 *    to void can be converted to this type, then converted back to pointer to void, and the result
 *    will compare equal to the original pointer:
 * -    uintptr_t
 *    These types are optional.
 */
typedef __INTPTR_TYPE__ intptr_t;
typedef __UINTPTR_TYPE__ uintptr_t;

/*
 * 7.20.1.5 Greatest-width integer types
 *
 * 1. The following type designates a signed integer capable of representing any value of any signed
 *    integer type:
 * -    intmax_t
 *    The following type designates an unsigned integer type capable of representing any value of
 *    any unsigned integer type:
 * -    uintmax_t
 *    The types are required.
 */
typedef __INTMAX_TYPE__ intmax_t;
typedef __UINTMAX_TYPE__ uintmax_t;

/*
 * 7.20.2 Widths of specified-width integer types
 *
 * 1. The following object-like macros specify the width of the types declared in <stdint.h>.
 *    Each macro name corresponds to a similar type name in 7.20.1.
 * 2. Each instance of any defined macros shall be replace by a constant expression suitable for
 *    use in #if preprocessing directives. Its implementation-defined value shall be equal to or
 *    greater that the value given below, except where stated to be exactly the given value.
 *    An implementation shall define only the macros corresponding to those typedef names it
 *    actually provides.
 */

/*
 * 7.20.2.1 Width of exact-width integer types
 * 1.
 * -    INTN_WIDTH          exactly N
 * -    UINTN_WIDTH         exactly N
 */
#define INT8_WIDTH         8
#define INT16_WIDTH        16
#define INT32_WIDTH        32
#define INT64_WIDTH        64

#define UINT8_WIDTH        8
#define UINT16_WIDTH       16
#define UINT32_WIDTH       32
#define UINT64_WIDTH       64

/*
 * 7.20.2.2 Width of minimum-width integer types
 * 1.
 * -    INT_LEASTN_WIDTH    exactly UINT_LEASTN_WIDTH
 * -    UINT_LEASTN_WIDTH   N
 */
#define UINT_LEAST8_WIDTH  8
#define UINT_LEAST16_WIDTH 16
#define UINT_LEAST32_WIDTH 32
#define UINT_LEAST64_WIDTH 64

#define INT_LEAST8_WIDTH   UINT_LEAST8_WIDTH
#define INT_LEAST16_WIDTH  UINT_LEAST16_WIDTH
#define INT_LEAST32_WIDTH  UINT_LEAST32_WIDTH
#define INT_LEAST64_WIDTH  UINT_LEAST64_WIDTH

/*
 * 7.20.2.3 Width of fastest minimum-width integer types
 * 1.
 * -    INT_FASTN_WIDTH     exactly UINT_FASTN_WIDTH
 * -    UINT_FASTN_WIDTH    N
 */
#define UINT_FAST8_WIDTH   8
#define UINT_FAST16_WIDTH  16
#define UINT_FAST32_WIDTH  32
#define UINT_FAST64_WIDTH  63

#define INT_FAST8_WIDTH    UINT_FAST8_WIDTH
#define INT_FAST16_WIDTH   UINT_FAST16_WIDTH
#define INT_FAST32_WIDTH   UINT_FAST32_WIDTH
#define INT_FAST64_WIDTH   UINT_FAST64_WIDTH

/*
 * 7.20.2.4 Width of integer types capable of holding object pointers
 * 1.
 * -    INTPTR_WIDTH        exactly UINTPTR_WIDTH
 * -    UINTPTR_WIDTH       16
 */
#ifdef __INTPTR_WIDTH__
#    define INTPTR_WIDTH __INTPTR_WIDTH__
#else
#    define INTPTR_WIDTH 32
#endif
#define UINTPTR_WIDTH INTPTR_WIDTH

/*
 * 7.20.2.4 Width of greatest-width integer types
 * 1.
 * -    INTMAX_WIDTH        exactly UINTMAX_WIDTH
 * -    UINTMAX_WIDTH       64
 */
#ifdef __INTMAX_WIDTH__
#    define INTMAX_WIDTH __INTMAX_WIDTH__
#else
#    define INTMAX_WIDTH 64
#endif
#define UINTMAX_WIDTH    INTMAX_WIDTH

/*
 * 7.20.3 Width of other integer types
 *
 * 1. The following object-like macros specify the width of integer types corresponding to types
 *    defined in other standard headers.
 * 2. Each instance of these macros shall be replaced by a constant expression suitable for use
 *    in #if preprocessing directives. Its implementation-defined value shall be equal to or greater
 *    that the corresponding value given below. An implementation shall define only the macros
 *    corresponding to those typedef names it actually provides.
 */

/*
 * 7.20.3.1 Width of ptrdiff_t
 * 1.
 * -    PTRDIFF_WIDTH       17
 */
#define PTRDIFF_WIDTH    __PTRDIFF_WIDTH__

/*
 * 7.20.3.2 Width of sig_atomic_t
 * 1.
 * -    SIG_ATOMIC_WIDTH    8
 */
#define SIG_ATOMIC_WIDTH __SIG_ATOMIC_WIDTH__

/*
 * 7.20.3.3 Width of size_t
 * 1.
 * -    SIZE_WIDTH          16
 */
#define SIZE_WIDTH       __SIZE_WIDTH__

/*
 * 7.20.3.4 Width of wchar_t
 * 1.
 * -    WCHAR_WIDTH         8
 */
#define WCHAR_WIDTH      __WCHAR_WIDTH__

/*
 * 7.20.3.5 Width of wint_t
 * -    WINT_WIDTH          16
 */
#define WINT_WIDTH       __WINT_WIDTH__

/*
 * 7.20.4 Macros for integer constants
 *
 * 1. The following function-like macros exapnd to integer constants suitable for initializing
 *    objects that have integer types corresponding to types defined in <stdint.h>. Each macro
 *    name corresponds to a similar type name in 7.20.1.2 or 7.20.1.5.
 * 2. The argument in any instance of these macros shall be an unsuffixed integer constant
 *    (as defined in 6.4.4.1) with a value that does not exceed the limits for the corresponding type.
 * 3. Each invocation of one of these macros shall expand to an integer constant expression suitable
 *    for use in #if preprocessing directives. The type of the expression shall have the same type as
 *    would an expression of the corresponding type converted according to the integer promotions. The
 *    value of the expression shall be that of the argument.
 */

/*
 * 7.20.4.1 Macros for minimum-width integer constants
 *
 * 1. The macros INTN_C(value) expands to an integer constant expression corresponding to the type
 *    int_leastN_t. The macro UINTN_C(value) expands to an integer constant expression corresponding
 *    to the type uint_lesatN_t. For example, if uint_least64_t is a name for the type
 *    unsigned lon long int, then UINT64_C(0x123) might expand to the integer constant 0x123ULL.
 */
#ifdef __INT8_C
#    define INT8_C(c) __INT8_C()
#else
#    define INT8_C(c) c
#endif
#ifdef __INT16_C
#    define INT16_C(c) __INT16_C(c)
#else
#    define INT16_C(c) c
#endif
#ifdef __INT32_C
#    define INT32_C(c) __INT32_C(c)
#else
#    define INT32_C(c) c
#endif
#ifdef __INT64_C
#    define INT64_C(c) __INT64_C(c)
#else
#    define INT64_C(c) c##LL
#endif

#ifdef __UINT8_C
#    define UINT8_C(c) __UINT8_C(c)
#else
#    define UINT8_C(c) c
#endif
#ifdef __UINT16_C
#    define UINT16_C(c) __UINT16_C(c)
#else
#    define UINT16_C(c) c
#endif
#ifdef __UINT32_C
#    define UINT32_C(c) __UINT32_C(c)
#else
#    define UINT32_C(c) c##U
#endif
#ifdef __UINT64_C
#    define UINT64_C(c) __UINT64_C(c)
#else
#    define UINT64_C(c) c##ULL
#endif

/*
 * 7.20.4.2 Macros for greatest width integer constants
 *
 * 1. The following macros expands to an integer constant expression having the value specified by
 *    its argument and the type intmax_t:
 * -    INTMAX_C(value)
 *    The following macro expands to an integer constant expression having the value specified by
 *    its argument and the type uintmax_t:
 *      UINTMAX_C(value)
 */
#ifdef __INTMAX_C
#    define INTMAX_C(c) __INTMAX_C(c)
#else
#    define INTMAX_C(c) c##LL
#endif
#ifdef __UINTMAX_C
#    define UINTMAX_C(c) __UINTMAX_C(c)
#else
#    define UINTMAX_C(c) c##ULL
#endif

/*
 * 7.20.5 Maximal and minimal values of integer types
 *
 * 1. For all integer types for which there is a macro with suffix _WIDTH holding the width,
 *    maximum macros with suffix _MAX and, for all signed types, minimum macros with suffix _MIN are
 *    defined as by 5.2.4.2. If it is unspecified if a type is signed or unsigned and the implementation
 *    has it as an unsigned type, a minimum macro with extension _MIN, and value 0 of the corresponding
 *    type is defined.
 */
#define INT8_MAX         (127)
#define INT8_MIN         (-127 - 1)
#define INT16_MAX        (32767)
#define INT16_MIN        (-32767 - 1)
#define INT32_MAX        (2147483647)
#define INT32_MIN        (-2147483647 - 1)
#define INT64_MAX        (INT64_C(9223372036854775807))
#define INT64_MIN        (INT64_C(9223372036854775807) - 1)

#define UINT8_MAX        (255)
#define UINT16_MAX       (65535)
#define UINT32_MAX       (4294967295U)
#define UINT64_MAX       (18446744073709551615ULL)

#define INT_LEAST8_MAX   INT8_MAX
#define INT_LEAST8_MIN   INT8_MIN
#define INT_LEAST16_MAX  INT16_MAX
#define INT_LEAST16_MIN  INT16_MIN
#define INT_LEAST32_MAX  INT32_MAX
#define INT_LEAST32_MIN  INT32_MIN
#define INT_LEAST64_MAX  INT64_MAX
#define INT_LEAST64_MIN  INT64_MIN

#define UINT_LEAST8_MAX  UINT8_MAX
#define UINT_LEAST16_MAX UINT16_MAX
#define UINT_LEAST32_MAX UINT32_MAX
#define UINT_LEAST64_MAX UINT64_MAX

#define INT_FAST8_MAX    INT8_MAX
#define INT_FAST8_MIN    INT8_MIN
#define INT_FAST16_MAX   INT16_MAX
#define INT_FAST16_MIN   INT16_MIN
#define INT_FAST32_MAX   INT32_MAX
#define INT_FAST32_MIN   INT32_MIN
#define INT_FAST64_MAX   INT64_MAX
#define INT_FAST64_MIN   INT64_MIN

#define UINT_FAST8_MAX   UINT8_MAX
#define UINT_FAST16_MAX  UINT16_MAX
#define UINT_FAST32_MAX  UINT32_MAX
#define UINT_FAST64_MAX  UINT64_MAX

#define INTPTR_MAX       __INTPTR_MAX__
#define INTPTR_MIN       (INTPTR_MAX - 1LL)
#define UINTPTR_MAX      __UINTPTR_MAX__

#define INTMAX_MAX       __INTMAX_MAX__
#define INTMAX_MIN       (INTMAX_MAX - 1LL)
#define UINTMAX_MAX      __UINTMAX_MAX__

#define PTRDIFF_MAX      __PTRDIFF_MAX__
#define PTRDIFF_MIN      (PTRDIFF_MAX - 1)

#define SIG_ATOMIC_MAX   __SIG_ATOMIC_MAX__
#define SIG_ATOMIC_MIN   (SIG_ATOMIC_MAX - 1)

#define SIZE_MAX         __SIZE_MAX__

#define WCHAR_MIN        __WCHAR_MIN__
#define WCHAR_MAX        __WCHAR_MAX__

#define WINT_MIN         __WINT_MIN__
#define WINT_MAX         __WINT_MAX__

#endif /* SOLC_STDINT_H */
