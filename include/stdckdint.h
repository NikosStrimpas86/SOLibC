#ifndef SOLC_STDCKDINT_H
#define SOLC_STDCKDINT_H

#include <stdbool.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

/*
 * 7.19 Checked Integer Arithmetic <stdckdint.h>
 *
 * 1. The header <stdckdint.h> defines several macros for performing checked integer arithmetic.
 */
/*
 * 7.19.1 The ckd_ Checked Integer Operation Macros
 *  Synopsis
 * 1. __________________________________________________
 *    | #include <stdckdint.h>                         |
 *    | bool ckd_add(type1 *result, type2 a, type3 b); |
 *    | bool ckd_sub(type1 *result, type2 a, type3 b); |
 *    | bool ckd_mul(type1 *result, type2 a, type3 b); |
 *    |________________________________________________|
 *  Description
 * 2. These macros perform addition, subtraction or multiplication of the mathematical values of
 *    a and b, storing the result of the operation in *result, (that is, *result is assigned the
 *    result of computing a + b, a - b, or a * b). Each operation is performed as if both operands
 *    were represented in a signed integer type with infinite range, and the result was then
 *    converted from this integer type to type1.
 * 3. Both type2 and type3 shall be any integer type other than plain char, bool a bit-precise
 *    integer type, or an enumeration type, and they need not be the same. *result shall be a
 *    modifiable lvalue of any integer type other than plain char, bool, a bit-precise integer
 *    type, or an enumeration type.
 *  Recommended practice
 * 4. It is recommended to produce a diagnostic message if type2 or type3 are not suitable integer
 *    types, or if *result is not a modifiable lvalue of a suitable integer type.
 *  Returns
 * 5. If these macros return false, the value assigned to *result correctly represents the
 *    mathematical result of the operation. Otherwise, these macros return true. In this case, the
 *    value assigned to *result is the mathematical result of the operation wrapped around to the
 *    width of *result.
 * 6. EXAMPLE 1: If a and b are values of type signed int, and result is a signed long, then
 * -    ckd_sub(&result, a, b);
 *    will indicate if a - b can be expressed as a signed long. If signed long has a greater width
 *    than signed int, this will always be possible and this macro will return false.
 */

#define __STDC_VERSION_STDCKDINT_H__ 202311L

/*
 * TODO: Provide a portable implementation.
 */

#if defined(__has_builtin)                   \
    && __has_builtin(__builtin_add_overflow) \
    && __has_builtin(__builtin_sub_overflow) \
    && __has_builtin(__builtin_mul_overflow)
#    define ckd_add(result, a, b) __builtin_add_overflow(a, b, result)
#    define ckd_sub(result, a, b) __builtin_sub_overflow(a, b, result)
#    define ckd_mul(result, a, b) __builtin_mul_overflow(a, b, result)
#else
#    error "<stdckdint.h> is not implemented"
#define _SOLC_USE_PORTABLE_IMPLEMENTATION 1
enum _Solc_ckd_res_info {
    _Solc_ckd_res_info_sc,
    _Solc_ckd_res_info_uc,
    _Solc_ckd_res_info_ss,
    _Solc_ckd_res_info_us,
    _Solc_ckd_res_info_si,
    _Solc_ckd_res_info_ui,
    _Solc_ckd_res_info_sl,
    _Solc_ckd_res_info_ul,
    _Solc_ckd_res_info_sll,
    _Solc_ckd_res_info_ull,
};

#    define __solc_ckdint_tag_result(VAL)                      \
        _Generic((VAL), signed char*                           \
                 : _Solc_ckd_res_info_sc, unsigned char*       \
                 : _Solc_ckd_res_info_uc, short*               \
                 : _Solc_ckd_res_info_ss, unsigned short*      \
                 : _Solc_ckd_res_info_us, int*                 \
                 : _Solc_ckd_res_info_si, unsigned int*        \
                 : _Solc_ckd_res_info_ui, long*                \
                 : _Solc_ckd_res_info_sl, unsigned long*       \
                 : _Solc_ckd_res_info_ul, long long*           \
                 : _Solc_ckd_res_info_sll, unsigned long long* \
                 : _Solc_ckd_res_info_ull)

bool __solc_ckd_addsllsll(void* res, enum _Solc_ckd_res_info info, long long a, long long b);
bool __solc_ckd_addsllull(void* res, enum _Solc_ckd_res_info info, long long a, unsigned long long b);
bool __solc_ckd_addullsll(void* res, enum _Solc_ckd_res_info info, unsigned long long a, long long b);
bool __solc_ckd_addullull(void* res, enum _Solc_ckd_res_info info, unsigned long long a, unsigned long long b);

bool __solc_ckd_subsllsll(void* res, enum _Solc_ckd_res_info info, long long a, long long b);
bool __solc_ckd_subsllull(void* res, enum _Solc_ckd_res_info info, long long a, unsigned long long b);
bool __solc_ckd_subullsll(void* res, enum _Solc_ckd_res_info info, unsigned long long a, long long b);
bool __solc_ckd_subullull(void* res, enum _Solc_ckd_res_info info, unsigned long long a, unsigned long long b);

bool __solc_ckd_mulsllsll(void* res, enum _Solc_ckd_res_info info, long long a, long long b);
bool __solc_ckd_mulsllull(void* res, enum _Solc_ckd_res_info info, long long a, unsigned long long b);
bool __solc_ckd_mulullsll(void* res, enum _Solc_ckd_res_info info, unsigned long long a, long long b);
bool __solc_ckd_mulullull(void* res, enum _Solc_ckd_res_info info, unsigned long long a, unsigned long long b);

/* clang-format off */

#    define __SOLC_CKD_IS_SIGNED(VAL) \
        _Generic((VAL)                \
            , signed char : 1         \
            , short : 1               \
            , int : 1                 \
            , long : 1                \
            , long long : 1           \
            , unsigned char : 0       \
            , unsigned short : 0      \
            , unsigned int : 0        \
            , unsigned long : 0       \
            , unsigned long long : 0  \
        )

#   define ckd_add(result, a, b)                                                   \
    _Generic(&(char[1 + __SOLC_CKD_IS_SIGNED(a) + 2 * __SOLC_CKD_IS_SIGNED(b)]){0} \
        , char(*)[4]: __solc_ckd_addsllsll                                         \
        , char(*)[3]: __solc_ckd_addullsll                                         \
        , char(*)[2]: __solc_ckd_addsllull                                         \
        , char(*)[1]: __solc_ckd_addullull                                         \
    )((result), __solc_ckdint_tag_result((result)), (a), (b))

#   define ckd_sub(result, a, b)                                                   \
    _Generic(&(char[1 + __SOLC_CKD_IS_SIGNED(a) + 2 * __SOLC_CKD_IS_SIGNED(b)]){0} \
        , char(*)[4]: __solc_ckd_subsllsll                                         \
        , char(*)[3]: __solc_ckd_subullsll                                         \
        , char(*)[2]: __solc_ckd_subsllull                                         \
        , char(*)[1]: __solc_ckd_subullull                                         \
    )((result), __solc_ckdint_tag_result((result)), (a), (b))

#   define ckd_mul(result, a, b)                                                   \
    _Generic(&(char[1 + __SOLC_CKD_IS_SIGNED(a) + 2 * __SOLC_CKD_IS_SIGNED(b)]){0} \
        , char(*)[4]: __solc_ckd_mulsllsll                                         \
        , char(*)[3]: __solc_ckd_mulullsll                                         \
        , char(*)[2]: __solc_ckd_mulsllull                                         \
        , char(*)[1]: __solc_ckd_mulullull                                         \
    )((result), __solc_ckdint_tag_result((result)), (a), (b))

/* clang-format on */

#endif

void __solc__internal_ckd_dummy_extern_function_do_not_touch(void);

__END_DECLS

#endif /* SOLC_STDCKDINT_H */
