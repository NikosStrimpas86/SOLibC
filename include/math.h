#ifndef SOLC_MATH_H
#define SOLC_MATH_H

#include <float.h>
#include <limits.h>
#include <sys/cdefs.h>

#define __STDC_VERSION_MATH_H__ 202111L /* FT */

__BEGIN_DECLS

#define M_E         2.7182818284590452354  /* e */
#define M_LOG2E     1.4426950408889634074  /* log_2(e) */
#define M_LOG10E    0.43429448190325182765 /* log_10(e) */
#define M_LN2       0.69314718055994530942 /* ln(2) */
#define M_LN10      2.30258509299404568402 /* ln(10) */
#define M_PI        3.14159265358979323846 /* pi */
#define M_PI_2      1.57079632679489661923 /* pi/2 */
#define M_PI_4      0.78539816339744830962 /* pi/4 */
#define M_1_PI      0.31830988618379067154 /* 1/pi */
#define M_2_PI      0.63661977236758134308 /* 2/pi */
#define M_2_SQRTPI  1.12837916709551257390 /* 2/sqrt(pi) */
#define M_SQRT2     1.41421356237309504880 /* sqrt(2) */
#define M_SQRT1_2   0.70710678118654752440 /* sqrt(1/2) */

#define M_Ef        2.7182818284590452354f  /* e */
#define M_LOG2Ef    1.4426950408889634074f  /* log_2(e) */
#define M_LOG10Ef   0.43429448190325182765f /* log_10(e) */
#define M_LN2f      0.69314718055994530942f /* ln(2) */
#define M_LN10f     2.30258509299404568402f /* ln(10) */
#define M_PIf       3.14159265358979323846f /* pi */
#define M_PI_2f     1.57079632679489661923f /* pi/2 */
#define M_PI_4f     0.78539816339744830962f /* pi/4 */
#define M_1_PIf     0.31830988618379067154f /* 1/pi */
#define M_2_PIf     0.63661977236758134308f /* 2/pi */
#define M_2_SQRTPIf 1.12837916709551257390f /* 2/sqrt(pi) */
#define M_SQRT2f    1.41421356237309504880f /* sqrt(2) */
#define M_SQRT1_2f  0.70710678118654752440f /* sqrt(1/2) */

#define M_El        2.718281828459045235360287471352662498L /* e */
#define M_LOG2El    1.442695040888963407359924681001892137L /* log_2(e) */
#define M_LOG10El   0.434294481903251827651128918916605082L /* log_10(e) */
#define M_LN2l      0.693147180559945309417232121458176568L /* ln(2) */
#define M_LN10l     2.302585092994045684017991454684364208L /* ln(10) */
#define M_PIl       3.141592653589793238462643383279502884L /* pi */
#define M_PI_2l     1.570796326794896619231321691639751442L /* pi/2 */
#define M_PI_4l     0.785398163397448309615660845819875721L /* pi/4 */
#define M_1_PIl     0.318309886183790671537767526745028724L /* 1/pi */
#define M_2_PIl     0.636619772367581343075535053490057448L /* 2/pi */
#define M_2_SQRTPIl 1.128379167095512573896158903121545172L /* 2/sqrt(pi) */
#define M_SQRT2l    1.414213562373095048801688724209698079L /* sqrt(2) */
#define M_SQRT1_2l  0.707106781186547524400844362104849039L /* sqrt(1/2) */

#if FLT_EVAL_METHOD == 0
typedef float float_t;
typedef double double_t;
#elif FLT_EVAL_METHOD == 1
typedef double float_t;
typedef double double_t;
#elif FLT_EVAL_METHOD == 2
typedef long double float_t;
typedef long double double_t;
#elif FLT_EVAL_METHOD == 32
#elif FLT_EVAL_METHOD == 64
#elif FLT_EVAL_METHOD == 128
#elif FLT_EVAL_METHOD == 33
#elif FLT_EVAL_METHOD == 65
#elif FLT_EVAL_METHOD == -1
#    error "Could not define standard suffixed floating point types"
#else
typedef float float_t;
typedef double double_t;
#endif /* FLT_EVAL_METHOD value */

#ifdef __STDC_IEC_60559_DFP__
#    if (DEC_EVAL_METHOD == 0)
typedef _Decimal32 _Decimal32_t;
typedef _Decimal64 _Decimal64_t;
#    elif (DEC_EVAL_METHOD == 1)
typedef _Decimal64 _Decimal32_t;
typedef _Decimal64 _Decimal64_t;
#    elif (DEC_EVAL_METHOD == 2)
typedef _Decimal128 _Decimal32_t;
typedef _Decimal128 _Decimal64_t;
#    else
typedef _Decimal32 _Decimal32_t;
typedef _Decimal64 _Decimal64_t;
#    endif /* DEC_EVAL_METHOD value */
#endif     /* __STDC_IEC_60559_DFP__ */

#define HUGE_VAL  __builtin_huge_val()
#define HUGE_VALF __builtin_huge_valf()
#define HUGE_VALL __builtin_huge_vall()

#ifdef __STDC_IEC_60559_DFP__
#    define HUGE_VAL_D32  // TODO: Add value
#    define HUGE_VAL_D64  // TODO: Add value
#    define HUGE_VAL_D128 // TODO: Add value
#endif

#if !(defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L))
#    define INFINITY     __builtin_inff()

#    define DEC_INFINITY __builtin_infd32()

#    define NAN          __builtin_nanf("")

#    define DEC_NAN      __builtin_nand32("")
#endif

/*
 * Powers of two allow for easy masking and manipulation.
 */
#define FP_INFINITE  1
#define FP_NAN       2
#define FP_NORMAL    4
#define FP_SUBNORMAL 8
#define FP_ZERO      16

#define FP_INT_UPWARD
#define FP_INT_DOWNWARD
#define FP_INT_TOWARDZERO
#define FP_INT_TONEARESTFROMZERO
#define FP_INT_TONEAREST

/*
 * FIXME: These should be defined as 1 for architectures that come with these instructions.
 */
#define FP_FAST_FMA         1
#define FP_FAST_FMAF        1
#define FP_FAST_FMAL        1
#define FP_FAST_FMAD32      1
#define FP_FAST_FMAD64      1
#define FP_FAST_FMAD128     1

#define FP_FAST_FADD        1
#define FP_FAST_FADDL       1
#define FP_FAST_DADDL       1
#define FP_FAST_FSUB        1
#define FP_FAST_FSUBL       1
#define FP_FAST_DSUBL       1
#define FP_FAST_FMUL        1
#define FP_FAST_FMULL       1
#define FP_FAST_DMULL       1
#define FP_FAST_FDIV        1
#define FP_FAST_FDIVL       1
#define FP_FAST_DDIVL       1
#define FP_FAST_FSQRT       1
#define FP_FAST_FSQRTL      1
#define FP_FAST_DSQRTL      1
#define FP_FAST_FFMA        1
#define FP_FAST_FFMAL       1
#define FP_FAST_DFMAL       1

#define FP_FAST_D32ADDD64   1
#define FP_FAST_D32ADDD128  1
#define FP_FAST_D64ADDD128  1
#define FP_FAST_D32SUBD64   1
#define FP_FAST_D32SUBD128  1
#define FP_FAST_D64SUBD128  1
#define FP_FAST_D32MULD64   1
#define FP_FAST_D32MULD128  1
#define FP_FAST_D64MULD128  1
#define FP_FAST_D32DIVD64   1
#define FP_FAST_D32DIVD128  1
#define FP_FAST_D64DIVD128  1
#define FP_FAST_D32FMAD64   1
#define FP_FAST_D32FMAD128  1
#define FP_FAST_D64FMAD128  1
#define FP_FAST_D32SQRTD64  1
#define FP_FAST_D32SQRTD128 1
#define FP_FAST_D64SQRTD128 1

#define FP_ILOGB0           (-INT_MAX)
#define FP_ILOGBNAN         INT_MIN

#define FP_LLOGB0           (-LONG_MAX)
#define FP_LLOGBNAN         LONG_MIN

int _fpclassify(double x);
int _fpclassifyf(float x);
int _fpclassifyl(long double x);

int _signbit(double x);
int _signbitf(float x);
int _signbitl(long double x);
#ifdef __STDC_IEC_60559_DFP__
int _signbitd32(_Decimal32 x);
int _signbitd64(_Decimal64 x);
int _signbitd128(_Decimal128 x);
/* FIXME: Add functions for _DecimalNx types too! */
#endif /* __STDC_IEC_60559_DFP__ */
#ifdef __STDC_IEC_60559_BFP__
int _signbitf16(_Float16 x);
int _signbitf32(_Float32 x);
int _signbitf64(_Float64 x);
int _signbitf128(_Float128 x);
/* FIXME: Add functions for _FloatNx types too! */
#endif /* __STDC_IEC_60559_BFP__ */

int _issignalingf(float x);
int _issignaling(double x);
int _issignalingl(long double x);

/*
 * TODO: Find a way to use both our implementation and intrisics.
 * FIXME: The C99 version should not work for non floating-point types.
 * TODO: Check whether or not it would be better to implement this by hand.
 * TODO: Check if it applies for decimal floating-point types too.
 * TODO: Check if it applies for binary floating-point types too.
 */
/* clang-format off */
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
#define __solc_fpclassify(x) _Generic((x)              \
                            , double: _fpclassify      \
                            , float: _fpclassifyf      \
                            , long double: _fpclassify \
                        )(x)
#else
#define __solc_fpclassify(x) (sizeof((x)) == sizeof(double) ? _fpclassify((x)) \
                     : sizeof((x)) == sizeof(float) ? _fpclassifyf((x))        \
                     : sizeof((x)) == sizeof(long double) ? _fpclassifyl((x))  \
                     : 0)
#endif
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
#define __solc_signbit(x) _Generic((x)           \
                        , double: _signbit       \
                        , float: _signbitf       \
                        , long double: _signbitl \
                    )(x)
#else
#define __solc_signbit(x) (sizeof((x)) == sizeof(double) ? _signbit((x)) \
                  : sizeof((x)) == sizeof(float) ? _signbitf((x))        \
                  : sizeof((x)) == sizeof(long double) ? _signbitl((x))  \
                  : -1)
#endif
#if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
#define __solc_issignaling(x) _Generic((x)              \
                            , float: _issignalingf      \
                            , double: _issignaling      \
                            , long double: _issignaling \
                        )(x)
#else
#define __solc_issignaling(x) (sizeof((x)) == sizeof(double) ? _issignaling((x)) \
                  : sizeof((x)) == sizeof(float) ? _issignalingf((x))            \
                  : sizeof((x)) == sizeof(long double) ? _issignalingl((x))      \
                  : 0)
#endif
/* clang-format on */

#define MATH_ERRNO       1
#define MATH_ERREXCEPT   2
#define math_errhandling MATH_ERRNO

#ifdef __STDC_IEC_60559_DFP__
#    define __SOLC_MATH_DFP_SELECTIONS(funcd32, funcd64, funcd128) \
        , _Decimal32 : funcd32, _Decimal64 : funcd64, _Decimal128 : funcd128
#else
#    define __SOLC_MATH_DFP_SELECTIONS(funcd32, funcd64, funcd128)
#endif
#ifdef __STDC_IEC_60559_BFP__
#    define __SOLC_MATH_BFP_SELECTIONS(funcf16, funcf32, funcf64, funcf128) \
        , _Float16 : funcf16, _Float32 : funcf32, _Float64 : funcf64, _Float128 : funcf128
#else
#    define __SOLC_MATH_BFP_SELECTIONS(funcf16, funcf32, funcf64, funcf128)
#endif

#define fpclassify(x) __solc_fpclassify(x)
#define iscanonical(x)
#define isfinite(x)    (fpclassify((x)) & (FP_ZERO | FP_SUBNORMAL | FP_NORMAL))
#define isinf(x)       (fpclassify((x)) == FP_INFINITE)
#define isnan(x)       (fpclassify((x)) == FP_NAN)
#define isnormal(x)    (fpclassify((x)) == FP_NORMAL)
#define signbit(x)     __solc_signbit(x)
#define issignaling(x) __solc_issignaling(x)
#define issubnormal(x) (fpclassify((x)) == FP_SUBNORMAL)
#define iszero(x)      (fpclassify((x)) == FP_ZERO)

double acos(double x);
float acosf(float x);
long double acosl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 acosd32(_Decimal32 x);
_Decimal64 acosd64(_Decimal64 x);
_Decimal128 acosd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double asin(double x);
float asinf(float x);
long double asinl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 asind32(_Decimal32 x);
_Decimal64 asind64(_Decimal64 x);
_Decimal128 asind128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double atan(double x);
float atanf(float x);
long double atanl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 atand32(_Decimal32 x);
_Decimal64 atand64(_Decimal64 x);
_Decimal128 atand128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double atan2(double y, double x);
float atan2f(float y, float x);
long double atan2l(long double y, long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 atan2d32(_Decimal32 x);
_Decimal64 atan2d64(_Decimal64 x);
_Decimal128 atan2d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double cos(double x);
float cosf(float x);
long double cosl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 cosd32(_Decimal32 x);
_Decimal64 cosd64(_Decimal64 x);
_Decimal128 cosd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double sin(double x);
float sinf(float x);
long double sinl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 sind32(_Decimal32 x);
_Decimal64 sind64(_Decimal64 x);
_Decimal128 sind128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double tan(double x);
float tanf(float x);
long double tanl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 tand32(_Decimal32 x);
_Decimal64 tand64(_Decimal64 x);
_Decimal128 tand128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double acospi(double x);
float acospif(float x);
long double acospil(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 acospid32(_Decimal32 x);
_Decimal64 acospid64(_Decimal64 x);
_Decimal128 acospid128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double asinpi(double x);
float asinpif(float x);
long double asinpil(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 asinpid32(_Decimal32 x);
_Decimal64 asinpid64(_Decimal64 x);
_Decimal128 asinpid128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double atanpi(double x);
float atanpif(float x);
long double atanpil(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 atanpid32(_Decimal32 x);
_Decimal64 atanpid64(_Decimal64 x);
_Decimal128 atanpid128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double tanpi(double x);
float tanpif(float x);
long double tanpil(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 tanpid32(_Decimal32 x);
_Decimal64 tanpid64(_Decimal64 x);
_Decimal128 tanpid128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double cospi(double x);
float cospif(float x);
long double cospil(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 cospid32(_Decimal32 x);
_Decimal64 cospid64(_Decimal64 x);
_Decimal128 cospid128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double sinpi(double x);
float sinpif(float x);
long double sinpil(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 sinpid32(_Decimal32 x);
_Decimal64 sinpid64(_Decimal64 x);
_Decimal128 sinpid128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double acosh(double x);
float acoshf(float x);
long double acoshl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 acoshd32(_Decimal32 x);
_Decimal64 acoshd64(_Decimal64 x);
_Decimal128 acoshd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double asinh(double x);
float asinhf(float x);
long double asinhl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 asinhd32(_Decimal32 x);
_Decimal64 asinhd64(_Decimal64 x);
_Decimal128 asinhd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double atanh(double x);
float atanhf(float x);
long double atanhl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 atanhd32(_Decimal32 x);
_Decimal64 atanhd64(_Decimal64 x);
_Decimal128 atanhd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double cosh(double x);
float coshf(float x);
long double coshl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 coshd32(_Decimal32 x);
_Decimal64 coshd64(_Decimal64 x);
_Decimal128 coshd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double sinh(double x);
float sinhf(float x);
long double sinhl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 sinhd32(_Decimal32 x);
_Decimal64 sinhd64(_Decimal64 x);
_Decimal128 sinhd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double tanh(double x);
float tanhf(float x);
long double tanhl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 tanhd32(_Decimal32 x);
_Decimal64 tanhd64(_Decimal64 x);
_Decimal128 tanhd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double exp(double x);
float expf(float x);
long double expl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 expd32(_Decimal32 x);
_Decimal64 expd64(_Decimal64 x);
_Decimal128 expd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double exp10(double x);
float exp10f(float x);
long double exp10l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 exp10d32(_Decimal32 x);
_Decimal64 exp10d64(_Decimal64 x);
_Decimal128 exp10d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double exp10m1(double x);
float exp10m1f(float x);
long double exp10m1l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 exp10m1d32(_Decimal32 x);
_Decimal64 exp10m1d64(_Decimal64 x);
_Decimal128 exp10m1d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double exp2(double x);
float exp2f(float x);
long double exp2l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 exp2d32(_Decimal32 x);
_Decimal64 exp2d64(_Decimal64 x);
_Decimal128 exp2d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double exp2m1(double x);
float exp2m1f(float x);
long double exp2m1l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 exp2m1d32(_Decimal32 x);
_Decimal64 exp2m1d64(_Decimal64 x);
_Decimal128 exp2m1d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double expm1(double x);
float expm1f(float x);
long double expm1l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 expm1d32(_Decimal32 x);
_Decimal64 expm1d64(_Decimal64 x);
_Decimal128 expm1d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double frexp(double x, int* p);
float frexpf(float x, int* p);
long double frexpl(long double x, int* p);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 frexpd32(_Decimal32 x, int* p);
_Decimal64 frexpd64(_Decimal64 x, int* p);
_Decimal128 frexpd128(_Decimal128 x, int* p);
#endif /* __STDC_IEC_60559_DFP__ */

int ilogb(double x);
int ilogbf(float x);
int ilogbl(long double x);
#ifdef __STDC_IEC_60559_DFP__
int ilogbd32(_Decimal32 x);
int ilogbd64(_Decimal64 x);
int ilogbd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double ldexp(double x, int p);
float ldexpf(float x, int p);
long double ldexpl(long double x, int p);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 ldexpd32(_Decimal32 x, int p);
_Decimal64 ldexpd64(_Decimal64 x, int p);
_Decimal128 ldexpd128(_Decimal128 x, int p);
#endif /* __STDC_IEC_60559_DFP__ */

long int llogb(double x);
long int llogbf(float x);
long int llogbl(long double x);
#ifdef __STDC_IEC_60559_DFP__
long int llogbd32(_Decimal32 x);
long int llogbd64(_Decimal64 x);
long int llogbd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double log(double x);
float logf(float x);
long double logl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 logd32(_Decimal32 x);
_Decimal64 logd64(_Decimal64 x);
_Decimal128 logd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double log10(double x);
float log10f(float x);
long double log10l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 log10d32(_Decimal32 x);
_Decimal64 log10d64(_Decimal64 x);
_Decimal128 log10d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double log10p1(double x);
float log10p1f(float x);
long double log10p1l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 log10p1d32(_Decimal32 x);
_Decimal64 log10p1d64(_Decimal64 x);
_Decimal128 log10p1d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double log1p(double x);
float log1pf(float x);
long double log1pl(long double x);
double logp1(double x);
float logp1f(float x);
long double logp1l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 log1pd32(_Decimal32 x);
_Decimal64 log1pd64(_Decimal64 x);
_Decimal128 log1pd128(_Decimal128 x);
_Decimal32 logp1d32(_Decimal32 x);
_Decimal64 logp1d64(_Decimal64 x);
_Decimal128 logp1d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double log2(double x);
float log2f(float x);
long double log2l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 log2d32(_Decimal32 x);
_Decimal64 log2d64(_Decimal64 x);
_Decimal128 log2d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double log2p1(double x);
float log2p1f(float x);
long double log2p1l(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 log2p1d32(_Decimal32 x);
_Decimal64 log2p1d64(_Decimal64 x);
_Decimal128 log2p1d128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double logb(double x);
float logbf(float x);
long double logbl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 logbd32(_Decimal32 x);
_Decimal64 logbd64(_Decimal64 x);
_Decimal128 logbd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double modf(double value, double* iptr);
float modff(float value, float* iptr);
long double modfl(long double value, long double* iptr);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 modfd32(_Decimal32 x, _Decimal32* iptr);
_Decimal64 modfd64(_Decimal64 x, _Decimal64* iptr);
_Decimal128 modfd128(_Decimal128 x, _Decimal128* iptr);
#endif /* __STDC_IEC_60559_DFP__ */

double scalbn(double x, int n);
float scalbnf(float x, int n);
long double scalbnl(long double x, int n);
double scalbln(double x, long int n);
float scalblnf(float x, long int n);
long double scalblnl(long double x, long int n);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 scalbnd32(_Decimal32 x, int n);
_Decimal64 scalbnd64(_Decimal64 x, int n);
_Decimal128 scalbnd128(_Decimal128 x, int n);
_Decimal32 scalblnd32(_Decimal32 x, long int n);
_Decimal64 scalblnd64(_Decimal64 x, long int n);
_Decimal128 scalblnd128(_Decimal128 x, long int n);
#endif /* __STDC_IEC_60559_DFP__ */

double cbrt(double x);
float cbrtf(float x);
long double cbrtl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 cbrtd32(_Decimal21 x);
_Decimal64 cbrtd64(_Decimal64 x);
_Decimal128 cbrtd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

#include <stdint.h>

double compoundn(double x, intmax_t n);
float compoundnf(float x, intmax_t n);
long double compoundnl(long double x, intmax_t n);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 compoundnd32(_Decimal32 x, intmax_t n);
_Decimal64 compoundnd64(_Decimal64 x, intmax_t n);
_Decimal128 compoundnd128(_Decimal128 x, intmax_t n);
#endif /* __STDC_IEC_60559_DFP__ */

double fabs(double x);
float fabsf(float x);
long double fabsl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fabsd32(_Decimal32 x);
_Decimal64 fabsd64(_Decimal64 x);
_Decimal128 fabsd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double hypot(double x, double y);
float hypotf(float x, float y);
long double hypotl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 hypotd32(_Decimal32 x, _Decimal64 y);
_Decimal64 hypotd64(_Decimal64 x, _Decimal64 y);
_Decimal128 hypotd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double pow(double x, double y);
float powf(float x, float y);
long double powl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 powd32(_Decimal32 x, _Decimal32 y);
_Decimal64 powd64(_Decimal64 x, _Decimal64 y);
_Decimal128 powd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double pown(double x, intmax_t n);
float pownf(float x, intmax_t n);
long double pownl(long double x, intmax_t n);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 pownd32(_Decimal32 x, intmax_t n);
_Decimal64 pownd64(_Decimal64 x, intmax_t n);
_Decimal128 pownd128(_Decimal128 x, intmax_t n);
#endif /* __STDC_IEC_60559_DFP__ */

double powr(double y, double x);
float powrf(float y, float x);
long double powrl(long double y, long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 powrd32(_Decimal32 y, _Decimal32 x);
_Decimal64 powrd64(_Decimal64 y, _Decimal64 x);
_Decimal128 powrd128(_Decimal128 y, _Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double rootn(double x, intmax_t n);
float rootnf(float x, intmax_t n);
long double rootnl(long double x, intmax_t n);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 rootnd32(_Decimal32 x, intmax_t n);
_Decimal64 rootnd64(_Decimal64 x, intmax_t n);
_Decimal128 rootnd128(_Decimal128 x, intmax_t n);
#endif /* __STDC_IEC_60559_DFP__ */

double rsqrt(double x);
float rsqrtf(float x);
long double rsqrtl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 rsqrtd32(_Decimal32 x);
_Decimal64 rsqrtd64(_Decimal64 x);
_Decimal128 rsqrtd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double sqrt(double x);
float sqrtf(float x);
long double sqrtl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 sqrtd32(_Decimal32 x);
_Decimal64 sqrtd64(_Decimal64 x);
_Decimal128 sqrtd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double erf(double x);
float erff(float x);
long double erfl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 erfd32(_Decimal32 x);
_Decimal64 erfd64(_Decimal64 x);
_Decimal128 erfd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double erfc(double x);
float erfcf(float x);
long double erfcl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 erfcd32(_Decimal32 x);
_Decimal64 erfcd64(_Decimal64 x);
_Decimal128 erfcd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double lgamma(double x);
float lgammaf(float x);
long double lgammal(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 lgammad32(_Decimal32 x);
_Decimal64 lgammad64(_Decimal64 x);
_Decimal128 lgammad128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double tgamma(double x);
float tgammaf(float x);
long double tgammal(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 tgammad32(_Decimal32 x);
_Decimal64 tgammad64(_Decimal64 x);
_Decimal128 tgammad128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double ceil(double x);
float ceilf(float x);
long double ceill(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 ceild32(_Decimal32 x);
_Decimal64 ceild64(_Decimal64 x);
_Decimal128 ceild128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double floor(double x);
float floorf(float x);
long double floorl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 floord32(_Decimal32 x);
_Decimal64 floord64(_Decimal64 x);
_Decimal128 floord128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double nearbyint(double x);
float nearbyintf(float x);
long double nearbyintl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nearbyintd32(_Decimal32 x);
_Decimal64 nearbyintd64(_Decimal64 x);
_Decimal128 nearbyintd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double rint(double x);
float rintf(float x);
long double rintl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 rintd32(_Decimal32 x);
_Decimal64 rintd64(_Decimal64 x);
_Decimal128 rintd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

long int lrint(double x);
long int lrintf(float x);
long int lrintl(long double x);
long long int llrint(double x);
long long int llrintf(float x);
long long int llrintl(long double x);
#ifdef __STDC_IEC_60559_DFP__
long int lrintd32(_Decimal32 x);
long int lrintd64(_Decimal64 x);
long int lrintd128(_Decimal128 x);
long long int llrintd32(_Decimal32 x);
long long int llrintd64(_Decimal64 x);
long long int llrintd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double round(double x);
float roundf(float x);
long double roundl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 roundd32(_Decimal32 x);
_Decimal64 roundd64(_Decimal64 x);
_Decimal128 roundd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

long int lround(double x);
long int lroundf(float x);
long int lroundl(long double x);
long long int llround(double x);
long long int llroundf(float x);
long long int llroundl(long double x);
#ifdef __STDC_IEC_60559_DFP__
long int lroundd32(_Decimal32 x);
long int lroundd64(_Decimal64 x);
long int lroundd128(_Decimal128 x);
long long int llroundd32(_Decimal32 x);
long long int llroundd64(_Decimal64 x);
long long int llroundd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double roundeven(double x);
float roundevenf(float x);
long double roundevenl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 roundevend32(_Decimal32 x);
_Decimal64 roundevend64(_Decimal64 x);
_Decimal128 roundevend128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double trunc(double x);
float truncf(float x);
long double truncl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 truncd32(_Decimal32 x);
_Decimal64 truncd64(_Decimal64 x);
_Decimal128 truncd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

intmax_t fromfp(double x, int round, unsigned int width);
intmax_t fromfpf(float x, int round, unsigned int width);
intmax_t fromfpl(long double x, int round, unsigned int width);
uintmax_t ufromfp(double x, int round, unsigned int width);
uintmax_t ufromfpf(float x, int round, unsigned int width);
uintmax_t ufromfpl(long double x, int round, unsigned int width);
#ifdef __STDC_IEC_60559_DFP__
intmax_t fromfpd32(_Decimal32 x, int round, unsigned int width);
intmax_t fromfpd64(_Decimal64 x, int round, unsigned int width);
intmax_t fromfpd128(_Decimal128 x, int round, unsigned int width);
uintmax_t ufromfpd32(_Decimal32 x, int round, unsigned int width);
uintmax_t ufromfpd64(_Decimal64 x, int round, unsigned int width);
uintmax_t ufromfpd128(_Decimal128 x, int round, unsigned int width);
#endif /* __STDC_IEC_60559_DFP__ */

intmax_t fromfpx(double x, int round, unsigned int width);
intmax_t fromfpxf(float x, int round, unsigned int width);
intmax_t fromfpxl(long double x, int round, unsigned int width);
uintmax_t ufromfpx(double x, int round, unsigned int width);
uintmax_t ufromfpxf(float x, int round, unsigned int width);
uintmax_t ufromfpxl(long double x, int round, unsigned int width);
#ifdef __STDC_IEC_60559_DFP__
intmax_t fromfpxd32(_Decimal32 x, int round, unsigned int width);
intmax_t fromfpxd64(_Decimal64 x, int round, unsigned int width);
intmax_t fromfpxd128(_Decimal128 x, int round, unsigned int width);
uintmax_t ufromfpxd32(_Decimal32 x, int round, unsigned int width);
uintmax_t ufromfpxd64(_Decimal64 x, int round, unsigned int width);
uintmax_t ufromfpxd128(_Decimal128 x, int round, unsigned int width);
#endif /* __STDC_IEC_60559_DFP__ */

double fmod(double x, double y);
float fmodf(float x, float y);
long double fmodl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmodd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmodd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmodd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double remainder(double x, double y);
float remainderf(float x, float y);
long double remainderl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 remainderd32(_Decimal32 x, _Decimal32 y);
_Decimal64 remainderd64(_Decimal64 x, _Decimal64 y);
_Decimal128 remainderd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double remquo(double x, double y, int* quo);
float remquof(float x, float y, int* quo);
long double remquol(long double x, long double y, int* quo);

double copysign(double x, double y);
float copysignf(float x, float y);
long double copysignl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 copysignd32(_Decimal32 x, _Decimal32 y);
_Decimal64 copysignd64(_Decimal64 x, _Decimal64 y);
_Decimal128 copysignd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double nan(const char* tagp);
float nanf(const char* tagp);
long double nanl(const char* tagp);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nand32(const char* tagp);
_Decimal64 nand64(const char* tagp);
_Decimal128 nand128(const char* tagp);
#endif /* __STDC_IEC_60559_DFP__ */

double nextafter(double x, double y);
float nextafterf(float x, float y);
long double nextafterl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nextafterd32(_Decimal32 x, _Decimal32 y);
_Decimal64 nextafterd64(_Decimal64 x, _Decimal64 y);
_Decimal128 nextafterd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double nexttoward(double x, long double y);
float nexttowardf(float x, long double y);
long double nexttowardl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nexttowardd32(_Decimal32 x, _Decimal128 y);
_Decimal64 nexttowardd64(_Decimal64 x, _Decimal128 y);
_Decimal128 nexttowardd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double nextup(double x);
float nextupf(float x);
long double nextupl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nextupd32(_Decimal32 x);
_Decimal64 nextupd64(_Decimal64 x);
_Decimal128 nextupd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

double nextdown(double x);
float nextdownf(float x);
long double nextdownl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nextdownd32(_Decimal32 x);
_Decimal64 nextdownd64(_Decimal64 x);
_Decimal128 nextdownd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

int canonicalize(double* cx, const double* x);
int canonicalizef(float* cx, const float* x);
int canonicalizel(long double* cx, const long double* x);
#ifdef __STDC_IEC_60559_DFP__
int canonicalized32(_Decimal32 cx, const _Decimal32* x);
int canonicalized64(_Decimal64 cx, const _Decimal64* x);
int canonicalized128(_Decimal128 cx, const _Decimal128* x);
#endif /* __STDC_IEC_60559_DFP__ */

double fdim(double x, double y);
float fdimf(float x, float y);
long double fdiml(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fdimd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fdimd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fdimd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fmax(double x, double y);
float fmaxf(float x, float y);
long double fmaxl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmaxd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmaxd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmaxd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fmin(double x, double y);
float fminf(float x, float y);
long double fminl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmind32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmind64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmind128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fmaximum(double x, double y);
float fmaximuf(float x, float y);
long double fmaximuml(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmaximumd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmaximumd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmaximumd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fminimum(double x, double y);
float fminimumf(float x, float y);
long double fminimuml(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fminimumd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fminimumd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fminimumd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fmaximum_mag(double x, double y);
float fmaximum_magf(float x, float y);
long double fmaximum_magl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmaximum_magd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmaximum_magd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmaximum_magd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fminimum_mag(double x, double y);
float fminimum_magf(float x, float y);
long double fminimum_magl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fminimum_magd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fminimum_magd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fminimum_magd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fmaximum_num(double x, double y);
float fmaximum_numf(float x, float y);
long double fmaximum_numl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmaximum_numd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmaximum_numd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmaximum_numd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fminimum_num(double x, double y);
float fminimum_numf(float x, float y);
long double fminimum_numl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fminimum_numd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fminimum_numd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fminimum_numd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fmaximum_mag_num(double x, double y);
float fmaximum_mag_numf(float x, float y);
long double fmaximum_mag_numl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmaximum_mag_numd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fmaximum_mag_numd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fmaximum_mag_numd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fminimum_mag_num(double x, double y);
float fminimum_mag_numf(float x, float y);
long double fminimum_mag_numl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fminimum_mag_numd32(_Decimal32 x, _Decimal32 y);
_Decimal64 fminimum_mag_numd64(_Decimal64 x, _Decimal64 y);
_Decimal128 fminimum_mag_numd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

double fma(double x, double y, double z);
float fmaf(float x, float y, float z);
long double fmal(long double x, long double y, long double z);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 fmad32(_Decimal32 x, _Decimal32 y, _Decimal32 z);
_Decimal64 fmad64(_Decimal64 x, _Decimal64 y, _Decimal64 z);
_Decimal128 fmad128(_Decimal128 x, _Decimal128 y, _Decimal128 z);
#endif /* __STDC_IEC_60559_DFP__ */

float fadd(double x, double y);
float faddl(long double x, long double y);
double daddl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 d32addd64(_Decimal64 x, _Decimal64 y);
_Decimal32 d32addd128(_Decimal128 x, _Decimal128 y);
_Decimal64 d64addd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

float fsub(double x, double y);
float fsubl(long double x, long double y);
double dsubl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 d32subd64(_Decimal64 x, _Decimal64 y);
_Decimal32 d32subd128(_Decimal128 x, _Decimal128 y);
_Decimal64 d64subd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

float fmul(double x, double y);
float fmull(long double x, long double y);
double dmull(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 d32muld64(_Decimal64 x, _Decimal64 y);
_Decimal32 d32muld128(_Decimal128 x, _Decimal128 y);
_Decimal64 d64muld128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

float fdiv(double x, double y);
float fdivl(long double x, long double y);
double ddivl(long double x, long double y);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 d32divd64(_Decimal64 x, _Decimal64 y);
_Decimal32 d32divd128(_Decimal128 x, _Decimal128 y);
_Decimal64 d64divd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

float ffma(double x, double y, double z);
float ffmal(long double x, long double y, long double z);
double dfmal(long double x, long double y, long double z);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 d32fmad64(_Decimal64 x, _Decimal64 y, _Decimal64 z);
_Decimal32 d32fmad128(_Decimal128 x, _Decimal128 y, _Decimal128 z);
_Decimal64 d64fmad128(_Decimal128 x, _Decimal128 y, _Decimal128 z);
#endif /* __STDC_IEC_60559_DFP__ */

float fsqrt(double x);
float fsqrtl(long double x);
double dsqrtl(long double x);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 d32sqrtd64(_Decimal64 x);
_Decimal32 d32sqrtd128(_Decimal128 x);
_Decimal64 d64sqrtd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
_Decimal32 quantized32(_Decimal32 x, _Decimal32 y);
_Decimal64 quantized64(_Decimal64 x, _Decimal64 y);
_Decimal128 quantized128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
_Bool samequantumd32(_Decimal32 x, _Decimal32 y);
_Bool samequantumd64(_Decimal64 x, _Decimal64 y);
_Bool samequantumd128(_Decimal128 x, _Decimal128 y);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
_Decimal32 quantumd32(_Decimal32 x);
_Decimal64 quantumd64(_Decimal64 x);
_Decimal128 quantumd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
long long int llquantexpd32(_Decimal32 x);
long long int llquantexpd64(_Decimal64 x);
long long int llquantexpd128(_Decimal128 x);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
void encodedecd32(unsigned char encptr[restrict static 4], const _Decimal32* restrict xptr);
void encodedecd64(unsigned char encptr[restrict static 8], const _Decimal64* restrict xptr);
void encodedecd128(unsigned char encptr[restrict static 16], const _Decimal128* restrict xptr);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
void decodedecd32(_Decimal32* restrict xptr, const unsigned char encptr[restrict static 4]);
void decodedecd64(_Decimal64* restrict xptr, const unsigned char encptr[restrict static 8]);
void decodedecd128(_Decimal128* restrict xptr, const unsigned char encptr[restrict static 16]);
#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_DFP__
void decodebind32(_Decimal32* restrict xptr, const unsigned char encptr[restrict static 4]);
void decodebind64(_Decimal64* restrict xptr, const unsigned char encptr[restrict static 8]);
void decodebind128(_Decimal128* restrict xptr, const unsigned char encptr[restrict static 16]);
#endif /* __STDC_IEC_60559_DFP__ */

int __iseqsig(double x, double y);
int __iseqsigf(float x, float y);
int __iseqsigl(long double x, long double y);

#define isgreater(x, y)      __builtin_isgreater((x), (y))
#define isgreaterequal(x, y) __builtin_isgreaterequal((x), (y))
#define isless(x, y)         __builtin_isless((x), (y))
#define islessequal(x, y)    __builtin_islessequal((x), (y))
#define islessgreater(x, y)  __builtin_islessgreater((x), (y))
#define isunordered(x, y)    __builtin_isunordered((x), (y))
#define iseqsig(x, y)

#ifdef __STDC_IEC_60559_BFP__
float getpayloadf(const float* x);
double getpayload(const double* x);
#endif
#ifdef __STDC_IEC_60559_DFP__
#endif

/* POSIX extensions */

double j0(double x);
double j1(double x);
double jn(int n, double x);

double y0(double x);
double y1(double x);
double yn(int n, double x);

__END_DECLS

#endif /* SOLC_MATH_H */
