#ifndef SOLC_STDALIGN_H
#define SOLC_STDALIGN_H

/*
 * TODO: Add preprocessor checks in order to not define alignas, alignof in C23 mode.
 */

/*
 * 7.15 Alignment <stdalign.h>
 *
 * 1. The header <stdalign.h> defines four macros
 * 2. The macro
 * -    alignas
 *    expands to _Alignas; the macro
 * -    alignof
 *    expands to _Alignof.
 * 3. The remaining macros are suitable for use in #if preprocessing directives. They are
 * -    __alignas_is_defined
 *    and
 * -    __alignof_is_defined
 *    which both expand to the integer constant 1
 */

#if !(defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L))

#define alignas _Alignas
#define alignof _Alignof

#endif

#define __alignas_is_defined 1
#define __alignof_is_defined 1

#endif /* SOLC_STDALIGN_H */
