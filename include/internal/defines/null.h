#ifndef SOLC_NULL_H
#define SOLC_NULL_H

#ifndef __cplusplus
#    define NULL ((void*)0)
#elif __cplusplus >= 201103L
#    define NULL nullptr
#else
#    define NULL 0L
#endif /* __cplusplus */

#endif /* SOLC_NULL_H */
