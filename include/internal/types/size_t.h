#ifndef SOLC_SIZE_T_H
#define SOLC_SIZE_T_H

typedef __SIZE_TYPE__ size_t;

#endif /* SOLC_SIZE_T_H */
