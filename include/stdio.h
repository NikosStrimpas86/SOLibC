#ifndef SOLC_STDIO_H
#define SOLC_STDIO_H

#include <stdarg.h>
#include <sys/cdefs.h>
/*
 * FIXME: Write correct things here.
 */
__BEGIN_DECLS
#include <internal/types/size_t.h>
/* FIXME: This is temporary */
typedef struct file_position {
    // mbstate_t parser_state;
    long c_position;
    long l_position;
} fpos_t;
/* FIXME: This is temporary*/
typedef struct temp_buffer {
    enum {
        Unset,
        Narrow,
        Wide
    } character_width;
    // mbstate_t parse_state;
    enum {
        Unbuffered,
        LineBuffered,
        FullyBuffered
    } buffering_state;
    enum {
        Input,
        Output,
        Update
    } io_mode;
    enum {
        Binary,
        Text
    } mode_indicator;
    _Bool end_of_file_status_indicator;
    int error_status_indicator;
    fpos_t file_position_indicator;
    _Bool reentrant_lock;
} FILE;

#include <internal/defines/null.h>
/* FIXME: define these correctly, according to the IO capabilities of our OS */
#define _IOFBF 0
#define _IOLBF 1
#define _IONBF 2

#define BUFSIZ 128
#ifndef EOF
#    define EOF (-1)
#endif
#define FOPEN_MAX           128
#define FILENAME_MAX        256
#define _PRINTF_NAN_LEN_MAX 32
#define L_tmpnam            128

#define SEEK_CUR            1
#define SEEK_END            2
#define SEEK_SET            3

#define TMP_MAX             256

extern FILE* stderr;
extern FILE* stdin;
extern FILE* stdout;

int remove(const char* filename);
int rename(const char* old, const char* new);
FILE* tmpfile(void);
char* tmpnam(char* s);

int fclose(FILE* stream);
int fflush(FILE* stream);
FILE* fopen(const char* restrict filename, const char* restrict mode);
FILE* freopen(const char* restrict filename, const char* restrict mode, FILE* restrict stream);
void setbuf(FILE* restrict stream, char* restrict buf);
int setvbuf(FILE* restrict stream, char* restrict buf, int mode, size_t size);

int fprintf(FILE* restrict stream, const char* restrict format, ...);
int fscanf(FILE* restrict stream, const char* restrict format, ...);
int printf(const char* restrict format, ...);
int scanf(const char* restrict format, ...);
int snprintf(char* restrict s, size_t n, const char* restrict format, ...);
int sprintf(char* restrict s, const char* restrict format, ...);
int sscanf(const char* restrict s, const char* restrict format, ...);
int vfprintf(FILE* restrict stream, const char* restrict format, va_list arg);
int vprintf(const char* restrict format, va_list arg);
int vscanf(const char* restrict format, va_list arg);
int vsnprintf(char* restrict s, size_t n, const char* restrict format, va_list arg);
int vsprintf(char* restrict s, const char* restrict format, va_list arg);
int vsscanf(const char* restrict s, const char* restrict format, va_list arg);

int fgetc(FILE* stream);
char* fgets(char* restrict s, int n, FILE* restrict stream);
int fputc(int c, FILE* stream);
int fputs(const char* restrict s, FILE* restrict stream);
int getc(FILE* stream);
int getchar(void);
int putc(int c, FILE* stream);
int putchar(int c);
int puts(const char* s);
int ungetc(int c, FILE* stream);

size_t fread(void* restrict ptr, size_t size, size_t nmemb, FILE* restrict stream);
size_t fwrite(const void* restrict ptr, size_t size, size_t nmbemb, FILE* restrict stream);
int fgetpos(FILE* restrict stream, fpos_t* restrict pos);
int fseek(FILE* stream, long int offset, int whence);
int fsetpos(FILE* stream, const fpos_t* pos);
long int ftell(FILE* stream);
void rewind(FILE* stream);

void clearerr(FILE* stream);
int feof(FILE* stream);
int ferror(FILE* stream);
void perror(const char* s);

__END_DECLS

#endif /* SOLC_STDIO_H */
