#ifndef SOLC_TYPES_H
#define SOLC_TYPES_H

/* POSIX extension */
/*
 * TODO: Define the pthread and trace related types
 *       These types may change in the future.
 */

#include <stdint.h>

/* clang-format off */
#ifndef __SOLC_BLKCNT_T_DEFINED
#define __SOLC_BLKCNT_T_DEFINED
typedef long blkcnt_t;
#endif

#ifndef __SOLC_BLKSIZE_T_DEFINED
#define __SOLC_BLKSIZE_T_DEFINED
typedef long blksize_t;
#endif

#ifndef __SOLC_CLOCK_T_DEFINED
#define __SOLC_CLOCK_T_DEFINED
typedef uint32_t clock_t;
#endif

#ifndef __SOLC_CLOCKID_T_DEFINED
#define __SOLC_CLOCKID_T_DEFINED
typedef int32_t clockid_t;
#endif

#ifndef __SOLC_DEV_T_DEFINED
#define __SOLC_DEV_T_DEFINED
typedef uint32_t dev_t;
#endif

#ifndef __SOLC_FSBLKCNT_T_DEFINED
#define __SOCL_FSBLKCNT_T_DEFINED
typedef uint64_t fsblkcnt_t;
#endif

#ifndef __SOLC_FSFILCNT_T_DEFINED
#define __SOLC_FSFILCNT_T_DEFINED
typedef uint64_t fsfilcnt_t;
#endif

#ifndef __SOLC_GID_T_DEFINED
#define __SOLC_GID_T_DEFINED
typedef uint32_t gid_t;
#endif

#ifndef __SOLC_ID_T_DEFINED
#define __SOLC_ID_T_DEFINED
typedef int64_t id_t;
#endif

#ifndef __SOLC_INO_T_DEFINED
#define __SOLC_INO_T_DEFINED
typedef unsigned long ino_t;
#endif

#ifndef __SOLC_KEY_T_DEFINED
#define __SOLC_KEY_T_DEFINED
typedef int32_t key_t;
#endif

#ifndef __SOLC_MODE_T_DEFINED
#define __SOLC_MODE_T_DEFINED
typedef uint16_t mode_t;
#endif

#ifndef __SOLC_NLINK_T_DEFINED
#define __SOLC_NLINK_T_DEFINED
typedef uint32_t nlink_t;
#endif

#ifndef __SOLC_OFF_T_DEFINED
#define __SOLC_OFF_T_DEFINED
typedef int64_t off_t;
#endif

#ifndef __SOLC_PID_T_DEFINED
#define __SOLC_PID_T_DEFINED
typedef int pid_t;
#endif

#include <internal/types/size_t.h>

#ifndef __SOLC_SSIZE_T_DEFINED
#define __SOLC_SSIZE_T_DEFINED
#   if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 202311L)
typedef typeof_unqual(
    _Generic((size_t)1
        , unsigned char: (signed char)1
        , unsigned short: (signed short)1
        , unsigned int: 1
        , unsigned long: 1l
        , unsigned long long: 1ll
        )
    ) ssize_t;
#   else
#       define unsigned signed
typedef __SIZE_TYPE__ ssize_t;
#       undef unsigned
#   endif
#endif

#ifndef __SOLC_SUSECONDS_T_DEFINED
#define __SOLC_SUSECONDS_T_DEFINED
typedef int32_t suseconds_t;
#endif

#ifndef __SOLC_TIME_T_DEFINED
#define __SOLC_TIME_T_DEFINED
typedef int64_t time_t;
#endif

#ifndef __SOLC_TIMER_T_DEFINED
#define __SOLC_TIMER_T_DEFINED
typedef int32_t timer_t;
#endif

#ifndef __SOLC_UID_T_DEFINED
#define __SOLC_UID_T_DEFINED
typedef uint32_t uid_t;
#endif

/* clang-format on */

#endif /* SOLC_TYPES_H */
