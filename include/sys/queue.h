#ifndef SOLC_QUEUE_H
#define SOLC_QUEUE_H

#include <internal/defines/_containerof.h>
#include <internal/defines/null.h>
/*
 * This is a BSD extension to the standard library.
 * https://www.freebsd.org/cgi/man.cgi?query=queue&manpath=FreeBSD+13.1-RELEASE+and+Ports
 * https://man.netbsd.org/queue.3
 * https://man7.org/linux/man-pages/man3/queue.3.html
 */

/*
 * C++ programs are allowed to use these data structures with both structs and classes.
 */
#ifdef __cplusplus
#define BSD_DOUBLE_PTR_OF_TYPE(T) T**
#define BSD_PTR_OF_TYPE(T)        T*
#define BSD_TYPE(T)               T
#else
#define BSD_DOUBLE_PTR_OF_TYPE(T) struct T**
#define BSD_PTR_OF_TYPE(T)        struct T*
#define BSD_TYPE(T)               struct T
#endif /* __cplusplus */

/* FIXME: Add debugging macros */

/* Singly linked list */

#define SLIST_HEAD(name, type)  \
    struct name {               \
        struct type* slh_first; \
    }

#define SLIST_HEAD_INITIALIZER(head) \
    {                                \
        NULL                         \
    }

#define SLIST_ENTRY(type)      \
    struct {                   \
        struct type* sle_next; \
    }

#ifdef __cplusplus
#define SLIST_CLASS_HEAD(name, type) \
    struct name {                    \
        class type* slh_first;       \
    }

#define SLIST_CLASS_ENTRY(type) \
    struct {                    \
        class type* sle_next;   \
    }
#endif /* __cplusplus */

#define SLIST_INIT(head)          \
    do {                          \
        (head)->slh_first = NULL; \
    } while (0)

#define SLIST_EMPTY(head)      ((head)->slh_first == NULL)
#define SLIST_FIRST(head)      ((head)->slh_first)
#define SLIST_END(head)        NULL
#define SLIST_NEXT(elm, field) ((elm)->field.sle_next)

#define SLIST_FOREACH(var, head, field) \
    for ((var) = (head)->slh_first; (var); (var) = (var)->field.sle_next)

#define SLIST_FOREACH_SAFE(var, head, field, tvar) \
    for ((var) = SLIST_FIRST((head)); (var) && ((tvar) = SLIST_NEXT((var), field), 1); (var) = (tvar))

#define SLIST_FOREACH_FROM(var, head, field) \
    for ((var) = ((var) ? (var) : (head)->slh_first); (var); (var) = (var)->field.sle_next)

#define SLIST_FOREACH_FROM_SAFE(var, head, field, tvar) \
    for ((var) = ((var) ? (var) : SLIST_FIRST((head))); (var) && ((tvar) = SLIST_NEXT((var), field), 1); (var) = (tvar))

#define SLIST_FOREACH_PREVPTR(var, varp, head, field) \
    for ((varp) = &SLIST_FIRST((head)); ((var) = *(var)) != NULL; (varp) = &SLIST_NEXT((var), field))

#define SLIST_INSERT_HEAD(head, elm, field)        \
    do {                                           \
        (elm)->field.sle_next = (head)->slh_first; \
        (head)->slh_first = (elm);                 \
    } while (0)

#define SLIST_INSERT_AFTER(slistelm, elm, field)            \
    do {                                                    \
        (elm)->field.sle_next = (slistelm)->field.sle_next; \
        (slistelm)->field.sle_next = (elm);                 \
    } while (0)

#define SLIST_REMOVE_HEAD(head, field)                         \
    do {                                                       \
        (head)->slh_first = (head)->slh_first->field.sle_next; \
    } while (0)

#define SLIST_REMOVE_AFTER(elm, field)                                 \
    do {                                                               \
        (elm)->field.sle_next = (elm)->field.sle_next->field.sle_next; \
    } while (0)

/* FIXME: Maybe elm should be deleted here? */
#define SLIST_REMOVE(head, elm, type, field)                                                                    \
    do {                                                                                                        \
        if ((head)->slh_first == (elm)) {                                                                       \
            SLIST_REMOVE_HEAD((head), field);                                                                   \
        } else {                                                                                                \
            BSD_PTR_OF_TYPE(type)                                                                               \
            _slist_iterator = (head)->slh_first;                                                                \
            for (; _slist_iterator->field.sle_next != (elm); _slist_iterator = _slist_iterator->field.sle_next) \
                ;                                                                                               \
            _slist_iterator->field.sle_next = _slist_iterator->field.sle_next->field.sle_next;                  \
        }                                                                                                       \
    } while (0)

#define SLIST_SWAP(head1, head2, type)               \
    do {                                             \
        BSD_PTR_OF_TYPE(type)                        \
        temp = SLIST_FIRST((head1));                 \
        SLIST_FIRST((head1)) = SLIST_FIRST((head2)); \
        SLIST_FIRST((head2)) = temp;                 \
    } while (0)

#define SLIST_CONCAT(head1, head2, type, field)                                                                      \
    do {                                                                                                             \
        BSD_PTR_OF_TYPE(type)                                                                                        \
        _slist_iterator = SLIST_FIRST((head1));                                                                      \
        if (_slist_iterator == NULL) {                                                                               \
            if ((SLIST_FIRST((head1)) = SLIST_FIRST((head2))) != NULL)                                               \
                SLIST_INIT((head2));                                                                                 \
        } else if (SLIST_FIRST((head2)) != NULL) {                                                                   \
            for (; SLIST_NEXT(_slist_iterator, field) != NULL; _slist_iterator = SLIST_NEXT(_slist_iterator, field)) \
                ;                                                                                                    \
            SLIST_NEXT(_slist_iterator, field) = SLIST_FIRST((head2));                                               \
            SLIST_INIT((head2));                                                                                     \
        }                                                                                                            \
    } while (0)

/* Linked List */

#define LIST_HEAD(name, type)  \
    struct name {              \
        struct type* lh_first; \
    }

#define LIST_HEAD_INITIALIZER \
    {                         \
        NULL                  \
    }

#define LIST_ENTRY(type)       \
    struct {                   \
        struct type* le_next;  \
        struct type** le_prev; \
    }

#ifdef __cplusplus
#define LIST_CLASS_HEAD(name, type) \
    struct name {                   \
        class type* lh_first;       \
    }

#define LIST_CLASS_ENTRY(name, type) \
    struct {                         \
        class type* le_next;         \
        class type** le_prev;        \
    }
#endif /* __cplusplus */

#define LIST_INIT(head)          \
    do {                         \
        (head)->lh_first = NULL; \
    } while (0)

#define LIST_FIRST(head)      ((head)->lh_first)
#define LIST_END(head)        NULL
#define LIST_EMPTY(head)      ((head)->lh_first == NULL)
#define LIST_NEXT(elm, field) ((elm)->field.le_next)
#define LIST_PREV(elm, head, type, field)        \
    (((elm)->field.le_prev == &(head)->lh_first) \
            ? NULL                               \
            : _containerof((elm)->field.le_prev, BSD_TYPE(type), field.le_next))

#define LIST_FOREACH(var, head, field) \
    for ((var) = (head)->lh_first; (var); (var) = (var)->field.le_next)

#define LIST_FOREACH_SAFE(var, head, field, tvar) \
    for ((var) = LIST_FIRST((head)); (var) && ((tvar) = LIST_NEXT((var), field), 1); (var) = (tvar))

#define LIST_FOREACH_FROM(var, head, field) \
    for ((var) = ((var) ? (var) : (head)->lh_first); (var); (var) = (var)->field.le_next)

#define LIST_FOREACH_FROM_SAFE(var, head, field, tvar) \
    for ((var) = ((var) ? (var) : LIST_FIRST((head))); (var) && ((tvar) = LIST_NEXT((var), field), 1); (var) = (tvar))

/* FIXME: Add kernel checks/panic in case of null head */
#define LIST_INSERT_HEAD(head, elm, field)                           \
    do {                                                             \
        if (((elm)->field.le_next = (head)->lh_first) != NULL)       \
            (head)->lh_first->field.le_prev = &(elm)->field.le_next; \
        (head)->lh_first = (elm);                                    \
        (elm)->field.le_prev = &(head)->lh_first;                    \
    } while (0)

/* FIXME: Add kernel checks/panic in case of null element */
#define LIST_INSERT_AFTER(listelm, elm, field)                                 \
    do {                                                                       \
        if (((elm)->field.le_next = (listelm)->field.le_next) != NULL)         \
            (listelm)->field.le_next->field.le_prev = &((elm)->field.le_next); \
        (listelm)->field.le_next = (elm);                                      \
        (elm)->field.le_prev = &(listelm)->field.le_next;                      \
    } while (0)

/* FIXME: Add kernel checks/panic in case of null element */
#define LIST_INSERT_BEFORE(listelm, elm, field)           \
    do {                                                  \
        (elm)->field.le_prev = (listelm)->field.le_prev;  \
        (elm)->field.le_next = (listelm);                 \
        *(listelm)->field.le_prev = (elm);                \
        (listelm)->field.le_prev = &(elm)->field.le_next; \
    } while (0)

/* FIXME: Add kernel checks/panic in case of null element */
#define LIST_REMOVE(elm, field)                                         \
    do {                                                                \
        if ((elm)->field.le_next != NULL)                               \
            (elm)->field.le_next->field.le_prev = (elm)->field.le_prev; \
        *(elm)->field.le_prev = (elm)->field.le_next                    \
    } while (0)

/* FIXME: Add kernel checks/panic in case of null element */
#define LIST_REPLACE(elm, elm2, field)                                     \
    do {                                                                   \
        if (((elm2)->field.le_next = (elm)->field.le_next) != NULL)        \
            (elm2)->field.le_next->field.le_prev = &(elm2)->field.le_next; \
        (elm2)->field.le_prev = (elm)->field.le_prev;                      \
        *(elm2)->field.le_prev = (elm2);                                   \
    } while (0)

#define LIST_SWAP(head1, head2, type, field)                \
    do {                                                    \
        BSD_PTR_OF_TYPE(type)                               \
        swap_tmp = LIST_FIRST((head1));                     \
        LIST_FIRST((head1)) = LIST_FIRST((head2));          \
        LIST_FIRST((head2)) = swap_tmp;                     \
        if ((swap_tmp = LIST_FIRST((head1))) != NULL)       \
            swap_tmp->field.le_prev = &LIST_FIRST((head1)); \
        if ((swap_tmp = LIST_FIRST((head2))) != NULL)       \
            swap_tmp->field.le_prev = &LIST_FIRST((head2)); \
    } while (0)

#define LIST_CONCAT(head1, head2, type, field)                                                                  \
    do {                                                                                                        \
        BSD_PTR_OF_TYPE(type)                                                                                   \
        _list_iterator = LIST_FIRST((head1));                                                                   \
        if (_list_iterator == NULL) {                                                                           \
            if ((LIST_FIRST((head1)) = LIST_FIRST((head2))) != NULL) {                                          \
                LIST_FIRST((head2))->field.le_prev = &LIST_FIRST((head1));                                      \
                LIST_INIT((head2));                                                                             \
            }                                                                                                   \
        } else if (LIST_FIRST((head2)) != NULL) {                                                               \
            for (; LIST_NEXT(_list_iterator, field) != NULL; _list_iterator = LIST_NEXT(_list_iterator, field)) \
                ;                                                                                               \
            LIST_NEXT(_list_iterator, field) = LIST_FIRST((head2));                                             \
            LIST_FIRST((head2))->field.le_prev = &LIST_NEXT(_list_iterator, field);                             \
            LIST_INIT((head2));                                                                                 \
        }                                                                                                       \
    } while (0)

/* Singly-linked tail queue */

#define STAILQ_HEAD(name, type)  \
    struct name {                \
        struct type* stqh_first; \
        struct type** stqh_last; \
    }

#define STAILQ_HEAD_INITIALIZER(head) \
    {                                 \
        NULL, &(head).stqh_first      \
    }

#define STAILQ_ENTRY(type)      \
    struct {                    \
        struct type* stqe_next; \
    }

#ifdef __cplusplus
#define STAILQ_CLASS_HEAD(name, type) \
    struct name {                     \
        class type* stqh_first;       \
        class type** stqh_last;       \
    }

#define STAILQ_CLASS_ENTRY(type) \
    struct {                     \
        class type* stqe_next;   \
    }
#endif /* __cplusplus */

#define STAILQ_INIT(head)                        \
    do {                                         \
        (head)->stqh_first = NULL;               \
        (head)->stqh_last = &(head)->stqh_first; \
    } while (0)

#define STAILQ_EMPTY(head)      ((head)->stqh_first == NULL)
#define STAILQ_FIRST(head)      ((head)->stqh_first)
#define STAILQ_END(head)        NULL
#define STAILQ_NEXT(elm, field) ((elm)->field.stqe_next)
#define STAILQ_LAST(head, type, field) \
    (((head)->stqh_first == NULL)      \
            ? NULL                     \
            : _containerof((head)->stqh_last, BSD_TYPE(type), field.stqe_next))

#define STAILQ_FOREACH(var, head, field) \
    for ((var) = ((head)->stqh_first); (var); (var) = ((var)->field.stqe_next))

#define STAILQ_FOREACH_FROM(var, head, field) \
    for ((var) = ((var) ? (var) : (head)->stqh_first); (var); (var) = ((var)->field.stqe_next))

#define STAILQ_FOREACH_SAFE(var, head, field, tvar) \
    for ((var) = STAILQ_FIRST((head)); (var) && ((tvar) = STAILQ_NEXT((var), field), 1); (var) = (tvar))

#define STAILQ_FOREACH_FROM_SAFE(var, head, field, tvar) \
    for ((var) = ((var) ? (var) : STAILQ_FIRST((head))); (var) && ((tvar) = STAILQ_NEXT((var), field), 1); (var) = (tvar))

#define STAILQ_INSERT_HEAD(head, elm, field)                         \
    do {                                                             \
        if (((elm)->field.stqe_next = (head)->stqh_first) == NULL) { \
            (head)->stqh_last = &(elm)->field.stqe_next;             \
        }                                                            \
        (head)->stqh_first = (elm);                                  \
    } while (0)

#define STAILQ_INSERT_AFTER(head, listelm, elm, field)                       \
    do {                                                                     \
        if (((elm)->field.stqe_next = (listelm)->field.stqe_next) == NULL) { \
            (head)->stqh_last = &(elm)->field.stqe_next;                     \
        }                                                                    \
        (tqelm)->field.stqe_next = (elm);                                    \
    } while (0)

#define STAILQ_INSERT_TAIL(head, elm, field)         \
    do {                                             \
        (elm)->field.stqe_next = NULL;               \
        *(head)->stqh_last = (elm);                  \
        (head)->stqh_last = &(elm)->field.stqe_next; \
    } while (0)

#define STAILQ_REMOVE_HEAD(head, field)                                           \
    do {                                                                          \
        if (((head)->stqh_first = (head)->stqh_first->field.stqe_next) == NULL) { \
            (head)->stqh_last = &(head)->stqh_first;                              \
        }                                                                         \
    } while (0)

#define STAILQ_REMOVE_AFTER(head, elm, field)                                             \
    do {                                                                                  \
        if (((elm)->field.stqe_next = (elm)->field.stqe_next->field.stqe_next) == NULL) { \
            (head)->stqh_last = &(elm)->field.stqe_next;                                  \
        }                                                                                 \
    } while (0)

/* FIXME: Add kernel checks/panic in case of null element */
#define STAILQ_REMOVE(head, elm, type, field)                                                                        \
    do {                                                                                                             \
        if ((head)->stqh_first == (elm)) {                                                                           \
            STAILQ_REMOVE_HEAD((head), field);                                                                       \
        } else {                                                                                                     \
            BSD_PTR_OF_TYPE(type)                                                                                    \
            _stailq_iterator = (head)->stqh_first;                                                                   \
            for (; _stailq_iterator->field.stqe_next != (elm); _stailq_iterator = _stailq_iterator->field.stqe_next) \
                ;                                                                                                    \
            STAILQ_REMOVE_AFTER((head), _stailq_iterator, field);                                                    \
        }                                                                                                            \
    } while (0)

/* FIXME: Add kernel checks/panic in case of null element */
#define STAILQ_SWAP(head1, head2, field)                 \
    do {                                                 \
        BSD_PTR_OF_TYPE(type)                            \
        _tmp_first = STAILQ_FIRST((head1));              \
        BSD_DOUBLE_PTR_OF_TYPE(type)                     \
        _tmp_last = (head1)->stqh_last;                  \
        STAILQ_FIRST((head1)) = STAILQ_FIRST((head2));   \
        (head1)->stqh_last = (head2)->stqh_last;         \
        STAILQ_FIRST((head2)) = _tmp_first;              \
        (head2)->stqh_last = _tmp_last;                  \
        if (STAILQ_EMPTY((head1))) {                     \
            (head1)->stqh_last = &STAILQ_FIRST((head1)); \
        }                                                \
        if (STAILQ_EMPTY((head2))) {                     \
            (head2)->stqh_last = &STAILQ_FIRST((head2))  \
        }                                                \
    } while (0)

#define STAILQ_CONCAT(head1, head2)                    \
    do {                                               \
        if (!STAILQ_EMPTY((head2))) {                  \
            *(head1)->stqh_last = (head2)->stqh_first; \
            (head1)->stqh_last = (head2)-stqh_last;    \
            STAILQ_INIT((head2));                      \
        }                                              \
    } while (0)

/* Simple queue */

#define SIMPLEQ_HEAD(name, type) \
    struct name {                \
        struct type* sqh_first;  \
        struct type** sqh_last   \
    }

#define SIMPLEQ_HEAD_INITIALIZER(head) \
    {                                  \
        NULL, &(head).sqh_first        \
    }

#define SIMPLEQ_ENTRY(type)    \
    struct {                   \
        struct type* sqe_next; \
    }

#ifdef __cplusplus
#define SIMPLEQ_CLASS_HEAD(name, type) \
    struct name {                      \
        class type* sqh_first;         \
        class type** sqh_last;         \
    }

#define SIMPLEQ_CLASS_ENTRY(type) \
    struct {                      \
        class type* sqe_next;     \
    }
#endif /* __cplusplus */

#define SIMPLEQ_INIT(head)                     \
    do {                                       \
        (head)->sqh_first = NULL;              \
        (head)->sqh_last = &(head)->sqh_first; \
    } while (0)

#define SIMPLEQ_FIRST(head)      ((head)->sqh_first)
#define SIMPLEQ_END(head)        NULL
#define SIMPLEQ_EMPTY(head)      ((head)->sqh_first == NULL)
#define SIMPLEQ_NEXT(elm, field) ((elm)->field.stqe_next)
#define SIMPLEQ_LAST(head, type, field) \
    (((head)->sqh_first == NULL)        \
            ? NULL                      \
            : _containerof((head)->sqh_last, BSD_TYPE(type), field.sqe_next))

#define SIMPLEQ_FOREACH(var, head, field) \
    for ((var) = ((head)->sqh_first); (var); (var) = ((var)->field.sqe_next))

#define SIMPLEQ_FOREACH_SAFE(var, head, field, tvar) \
    for ((var) = SIMPLEQ_FIRST((head)); (var) && ((tvar) = SIMPLEQ_NEXT((var), field), 1); (var) = (tvar))

#define SIMPLEQ_INSERT_HEAD(head, elm, field)                      \
    do {                                                           \
        if (((elm)->field.sqe_next = (head)->sqh_first) == NULL) { \
            (head)->sqh_last = &(elm)->field.sqe_next;             \
        }                                                          \
        (head)->sqh_first = (elm);                                 \
    } while (0)

#define SIMPLEQ_INSERT_AFTER(head, listelm, elm, field)                    \
    do {                                                                   \
        if (((elm)->field.sqe_next = (listelm)->field.sqe_next) == NULL) { \
            (head)->sqh_last = &(elm)->field.sqe_next;                     \
        }                                                                  \
        (listelm)->field.sqe_next = (elm);                                 \
    } while (0)

#define SIMPLEQ_INSERT_TAIL(head, elm, field)      \
    do {                                           \
        (elm)->field.sqe_next = NULL;              \
        *(head)->sqh_last = (elm);                 \
        (head)->sqh_last = &(elm)->field.sqe_next; \
    } while (0)

#define SIMPLEQ_REMOVE_HEAD(head, field)                                       \
    do {                                                                       \
        if (((head)->sqh_first = (head)->sqh_first->field.sqe_next) == NULL) { \
            (head)->sqh_last = &(head)->sqh_first;                             \
        }                                                                      \
    } while (0)

#define SIMPLEQ_REMOVE_AFTER(head, elm, field)                                         \
    do {                                                                               \
        if (((elm)->field.sqe_next = (elm)->field.sqe_next->field.sqe_next) == NULL) { \
            (head)->sqh_last = &(elm)->field.sqe_next;                                 \
        }                                                                              \
    } while (0)

/* FIXME: (Possibly) Add kernel checks/panic in case of null element */
#define SIMPLEQ_REMOVE(head, elm, type, field)                                                                        \
    do {                                                                                                              \
        if ((head)->sqh_first == (elm)) {                                                                             \
            SIMPLEQ_REMOVE_HEAD((head), field);                                                                       \
        } else {                                                                                                      \
            BSD_PTR_OF_TYPE(type)                                                                                     \
            _simpleq_iterator = (head)->sqh_first;                                                                    \
            for (; _simpleq_iterator->field.sqe_next != (elm); _simpleq_iterator = _simpleq_iterator->field.sqe_next) \
                ;                                                                                                     \
            SIMPLEQ_REMOVE_AFTER((head), _simpleq_iterator, field);                                                   \
        }                                                                                                             \
    } while (0)

#define SIMPLEQ_CONCAT(head1, head2)                 \
    do {                                             \
        if (!SIMPLEQ_EMPTY((head2))) {               \
            *(head1)->sqh_last = (head2)->sqh_first; \
            (head1)->sqh_last = (head2)->sqh_last;   \
            SIMPLEQ_INIT((head2));                   \
        }                                            \
    } while (0)

/* Circular queue */

#define CIRCLEQ_HEAD(name, type) \
    struct name {                \
        struct type* cqh_first;  \
        struct type* cqh_last;   \
    }

#define CIRCLEQ_HEAD_INITIALIZER(head) \
    {                                  \
        (void*)&head, (void*)&head     \
    }

#define CIRCLEQ_ENTRY(type)    \
    struct {                   \
        struct type* cqe_next; \
        struct type* cqe_prev; \
    }

#ifdef __cplusplus
#define CIRCLEQ_CLASS_HEAD(name, type) \
    struct name {                      \
        class type* cqh_first;         \
        class type* cqh_last;          \
    }

#define CIRCLEQ_CLASS_ENTRY(type) \
    struct {                      \
        class type* cqe_next;     \
        class type* cqe_prev;     \
    }
#endif /* __cplusplus */

#define CIRCLEQ_INIT(head)                 \
    do {                                   \
        (head)->cqe_first = (void*)(head); \
        (head)->cqe_last = (void*)(head);  \
    } while (0)

#define CIRCLEQ_EMPTY(head)      ((head)->cqh_first == (void*)(head))
#define CIRCLEQ_FIRST(head)      ((head)->cqh_first)
#define CIRCLEQ_LAST(head)       ((head)->cqh_last)
#define CIRCLEQ_NEXT(elm, field) ((elm)->field.cqe_next)
#define CIRCLEQ_PREV(elm, field) ((elm)->field.cqe_prev)
#define CIRCLEQ_LOOP_PREV(head, elm, field)   \
    (((elm)->field.cqe_prev == (void*)(head)) \
            ? ((head)->cqh_last)              \
            : ((elm)->field.cqe_prev))
#define CIRCLEQ_LOOP_NEXT(head, elm, field)   \
    (((elm)->field.cqe_next == (void*)(head)) \
            ? ((head)->cqh_first)             \
            : ((elm)->field.cqe_next))

#define CIRCLEQ_FOREACH(var, head, field) \
    for ((var) = ((head)->cqh_first); (var) != (const void*)head; (var) = ((var)->field.cqe_next))

#define CIRCLEQ_FOREACH_REVERSE(var, head, field) \
    for ((var) = ((head)->cqh_last); (var) != (const void*)head; (var) = ((var)->field.cqe_prev))

/* FIXME: (Possibly) Add kernel checks/panic in case of null element */
#define CIRCLEQ_INSERT_HEAD(head, elm, field)          \
    do {                                               \
        (elm)->field.cqe_next = (head)->cqh_first;     \
        (elm)->field.cqe_prev = (void*)(head);         \
        if ((head)->cqh_last == (void*)(head)) {       \
            (head)->cqh_first = (elm);                 \
        } else {                                       \
            (head)->cqh_first->field.cqe_prev = (elm); \
        }                                              \
        (head)->cqh_first = (elm);                     \
    } while (0)

/* FIXME: (Possibly) Add kernel checks/panic in case of null element */
#define CIRCLEQ_INSERT_TAIL(head, elm, field)         \
    do {                                              \
        (elm)->field.cqe_next = (void*)(head);        \
        (elm)->field.cqe_prev = (head)->cqh_last;     \
        if ((head)->cqh_first == (void*)(head)) {     \
            (head)->cqh_first = (elm);                \
        } else {                                      \
            (head)->cqh_last->field.cqe_next = (elm); \
        }                                             \
        (head)->cqh_last = (elm);                     \
    } while (0)

/* FIXME: (Possibly) Add kernel checks/panic in case of null element */
#define CIRCLEQ_INSERT_AFTER(head, listelm, elm, field)        \
    do {                                                       \
        (elm)->field.cqe_next = (listelm)->field.cqe_next;     \
        (elm)->field.cqe_prev = (listelm);                     \
        if ((listelm)->field.cqe_next == (void*)(head)) {      \
            (head)->cqh_last = (elm);                          \
        } else {                                               \
            (listelm)->field.cqe_next->field.cqe_prev = (elm); \
        }                                                      \
        (listelm)->field.cqe_next = (elm);                     \
    } while (0)

/* FIXME: (Possibly) Add kernel checks/panic in case of null element */
#define CIRCLEQ_INSERT_BEFORE(head, listelm, elm, field)       \
    do {                                                       \
        (elm)->field.cqe_next = (listelm);                     \
        (elm)->field.cqe_prev = (listelm)->field.cqe_prev;     \
        if ((listelm)->field.cqe_prev == (void*)(head)) {      \
            (head)->cqh_first = (elm);                         \
        } else {                                               \
            (listelm)->field.cqe_prev->field.cqe_next = (elm); \
        }                                                      \
        (listelm)->field.cqe_prev = (elm);                     \
    } while (0)

/* FIXME: (Possibly) Add kernel checks/panic in case of null element */
#define CIRCLEQ_REMOVE(head, elm, field)                                   \
    do {                                                                   \
        if ((elm)->field.cqe_next == (void*)(head)) {                      \
            (head)->cqh_last = (elm)->field.cqe_prev;                      \
        } else {                                                           \
            (elm)->field.cqe_next->field.cqe_prev = (elm)->field.cqe_prev; \
        }                                                                  \
        if ((elm)->field.cqe_prev == (void*)(head)) {                      \
            (head)->cqh_first = (elm)->field.cqe_next;                     \
        } else {                                                           \
            (elm)->field.cqe_prev->field.cqe_next = (elm)->field.cqe_next; \
        }                                                                  \
    } while (0)

#endif /* SOLC_QUEUE_H */
