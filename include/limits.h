#ifndef SOLC_LIMITS_H
#define SOLC_LIMITS_H

/*
 * 7.10 Characteristics of integer types <limits.h>
 *
 * 1. The header <limits.h> defines several macros that expand to various limits
 *    and parameters of the standard integer types.
 * 2. The macros, their meanings, and the constraints (or restrictions) on
 *    their values are listed in 5.2.4.2.1.
 *    A summary is given in Annex E.
 *
 */

/*
 * 5.2.4.2.1 Sizes of integer types <limits.h>
 *
 * 1. The values given below shall be replaced by constant expressions suitable
 *    for use in #if preprocessing directives. Their implementation-defined values
 *    shall be equal or greater to those shown.
 */

/*
 * - width for an object of type _Bool
 * -    BOOL_WIDTH      1
 */
#ifdef __BOOL_WIDTH__
#define BOOL_WIDTH __BOOL_WIDTH__
#else
#define BOOL_WIDTH       (1)
#endif

/*
 * - number of bits for smallest object that is not a bit-field (byte)
 * -    CHAR_WIDTH      8
 * The macros CHAR_WIDTH, SCHAR_WIDTH, and UCHAR_WIDTH that represent the width of the
 * types char, signed char and unsigned char shall expand to the same value as CHAR_BIT.
 */
#ifdef __CHAR_BIT__
#define CHAR_BIT __CHAR_BIT__
#else
#define CHAR_BIT         (8)
#endif
#define CHAR_WIDTH       CHAR_BIT
#ifdef __SCHAR_WIDTH__
#define SCHAR_WIDTH __SCHAR_WIDTH__
#define UCHAR_WIDTH SCHAR_WIDTH
#else
#define SCHAR_WIDTH      CHAR_BIT
#define UCHAR_WIDTH      CHAR_BIT
#endif


/*
 * - width for an object of type unsigned short int
 * -    USHRT_WIDTH     16
 * The macro SHRT_WIDTH represents the width of the type short int and shall expand
 * to the same value as USHRT_WIDTH.
 */
#ifdef __SHRT_WIDTH__
#define SHRT_WIDTH __SHRT_WIDTH__
#define USHRT_WIDTH SHRT_WIDTH
#else
#define USHRT_WIDTH      (16)
#define SHRT_WIDTH       USHRT_WIDTH
#endif

/*
 * - width for an object of type unsigned int
 * -    UINT_WIDTH      16
 * The macro INT_WIDTH represents the width of the type int and shall expand to the
 * same value as UINT_WIDTH
 */
#ifdef __INT_WIDTH__
#define INT_WIDTH __INT_WIDTH__
#define UINT_WIDTH INT_WIDTH
#else
#define UINT_WIDTH       (32)
#define INT_WIDTH        UINT_WIDTH
#endif

/*
 * - width for an object of type unsigned long int
 * -    ULONG_WIDTH     32
 * The macro ULONG_WIDTH represents the width of the type long int and shall expand
 * to the same value as ULONG_WIDTH
 */
#ifdef __LONG_WIDTH__
#define LONG_WIDTH __LONG_WIDTH__
#define ULONG_WIDTH LONG_WIDTH
#else
#define ULONG_WIDTH      (32)
#define LONG_WIDTH       ULONG_WIDTH
#endif

/*
 * - width for an object of type unsigned long long int
 * -    ULLONG_WIDTH    64
 * The macro ULLONG_WIDTH represents the width of the type long long int and shall
 * expand to the same value as ULLONG_WIDTH
 */
#ifdef __LONG_LONG_WIDTH__
#define LLONG_WIDTH __LONG_LONG_WIDTH__
#define ULLONG_WIDTH LLONG_WIDTH
#else
#define ULLONG_WIDTH     (64)
#define LLONG_WIDTH      ULLONG_WIDTH
#endif

/*
 * - maximum width for an object of type _BitInt or unsigned _BitInt
 * -    BITINT_MAXWIDTH // see below
 * The macro BITINT_MAXWIDTH represents the maximum width N supported by the declaration
 * of a bit-precise integer (6.2.5) in the type specifier _BitInt(N). The value BITINT_MAXWIDTH
 * shall expand to a value that is greater than or equal to the value of ULLONG_WIDTH.
 */
#ifdef __BITINT_MAXWIDTH__
#define BITINT_MAXIWDTH __BITINT_MAXWIDTH__
#else
#define BITINT_MAXWIDTH  (128)
#endif

/*
 * - maximum number of bytes in a multibyte character, for any supported locale
 * -    MB_LEN_MAX      1
 */
#define MB_LEN_MAX       (16)

/*
 * 2. For all unsigned integer types for which <limits.h> or <stdint.h> define a macro
 *    with suffix _WIDTH holding its width N, there is a macro with suffix _MAX holding
 *    the maximal value 2^N-1 that is representable by the type, that is suitable for
 *    use in #if preprocessing directives and that has the same type as would an
 *    expression that is an object of the corresponding type converted according to the
 *    integer promotions.
 * 3. For all signed integer types for which <limits.h> or <stdint.h> define a macro with
 *    suffix _WIDTH holding its width N, there are macros with suffix _MIN and _MAX
 *    holding the minimal and maximal values -2^(N-1) and 2^(N-1)-1 that are representable by
 *    the type, that are suitable for use in #if preprocessing directives and that have the
 *    same type as would an expression that is an object of the corresponding type converted
 *    according to the integer promotions.
 * 4. If an object of type char can hold negative values, the value of CHAR_MIN shall be the
 *    same as that of SCHAR_MIN and the value of CHAR_MAX shall be the same as that of SCHAR_MAX.
 *    Otherwise, the value of CHAR_MIN shall be 0 and the value of CHAR_MAX shall be the same
 *    as that of UCHAR_MAX
 */

#define BOOL_MAX         (1)

#define SCHAR_MAX        (127)
#define SCHAR_MIN        (-1 - SCHAR_MAX)
#define UCHAR_MAX        (255)

/* We assume that char is singed for the time being */
#define CHAR_MAX         (127)
#define CHAR_MIN         (-1 - CHAR_MAX)

#define SHRT_MAX         (32767)
#define SHRT_MIN         (-1 - SHRT_MAX)

#define USHRT_MAX        (65535)

#define INT_MAX          (2147483647)
#define INT_MIN          (-1 - INT_MAX)

#define UINT_MAX         (4294967295U)

/*
 * TODO: The OS is probably going to be just another POSIX/UNIX thing,
 *       so long will probably be 64 bits.
 */
#define LONG_MAX         (2147483647L)
#define LONG_MIN         (-1L - LONG_MAX)

#define ULONG_MAX        (4294967295UL)

#define LLONG_MAX        (9223372036854775807LL)
#define LLONG_MIN        (-1LL - LLONG_MAX)

#define ULLONG_MAX       (18446744073709551615ULL)

/*
 * Forward references: representations of types (6.2.6), conditional inclusion (6.10.1),
 * integer types <stdint.h> (7.20).
 */

#endif /* SOLC_LIMITS_H */
