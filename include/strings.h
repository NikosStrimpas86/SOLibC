#ifndef SOLC_STRINGS_H
#define SOLC_STRINGS_H

#include <internal/types/size_t.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

/*
 * TODO: Add the gnu extensions of this header.
 */

/* POSIX extension */

int ffs(int i);
int strcasecmp(char const* s1, char const* s2);
int strncasecmp(char const* s1, char const* s2, size_t n);

/*
 * WARNING: Interface marked as LEGACY
 * TODO: Write preprocessor magic code to restrict access for new POSIX versions
 */
int bcmp(void const* s1, void const* s2, size_t n);
void bzero(void* s, size_t n);
void bcopy(void const* s1, void* s2, size_t n);
char* index(char const* s, int c);
char* rindex(char const* s, int c);

/* IDW extension */
int ffsl(long i);
int ffsll(long long i);
int fls(int i);
int flsl(long i);
int flsll(long long i);

__END_DECLS

#endif /* SOLC_STRINGS_H */
