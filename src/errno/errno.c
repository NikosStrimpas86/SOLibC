#include <errno.h>

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)
_Thread_local int __errno_current = 0;
#else
int __errno_current = 0;
#endif

int* __errno_location()
{
    return &__errno_current;
}
