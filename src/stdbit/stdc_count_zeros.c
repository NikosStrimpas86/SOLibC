#include <stdbit.h>
#include <limits.h>

int stdc_count_zerosuc(unsigned char value)
{
    return stdc_count_onesuc((~value) & UCHAR_MAX);
}

int stdc_count_zerosus(unsigned short value)
{
    return stdc_count_onesus((~value) & USHRT_MAX);
}

int stdc_count_zerosui(unsigned int value)
{
    return stdc_count_onesui(~value);
}

int stdc_count_zerosul(unsigned long value)
{
    return stdc_count_onesul(~value);
}

int stdc_count_zerosull(unsigned long long value)
{
    return stdc_count_onesull(~value);
}

