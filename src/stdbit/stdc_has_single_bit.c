#include <limits.h>
#include <stdbit.h>

_Bool stdc_has_single_bituc(unsigned char value)
{
    return stdc_count_onesuc(value) == 1;
}

_Bool stdc_has_single_bitus(unsigned short value)
{
    return stdc_count_onesus(value) == 1;
}

_Bool stdc_has_single_bitui(unsigned int value)
{
    return stdc_count_onesui(value) == 1;
}

_Bool stdc_has_single_bitul(unsigned long value)
{
    return stdc_count_onesul(value) == 1;
}

_Bool stdc_has_single_bitull(unsigned long long value)
{
    return stdc_count_onesull(value) == 1;
}
