#include <stdbit.h>
#include <limits.h>

int stdc_trailing_onesuc(unsigned char value)
{
#if UCHAR_WIDTH == 8
    unsigned char comp = (~value) & 0xff;
    return stdc_trailing_zerosuc(comp);
#else
    return stdc_trailing_zerosuc(~value);
#endif
}

int stdc_trailing_onesus(unsigned short value)
{
#if USHRT_WIDTH == 16
    unsigned short comp = (~value) & 0xffff;
    return stdc_trailing_zerosus(comp);
#else
    return stdc_trailing_zerosus(~value);
#endif
}

int stdc_trailing_onesui(unsigned int value)
{
    return stdc_trailing_zerosui(~value);
}

int stdc_trailing_onesul(unsigned long value)
{
    return stdc_trailing_zerosul(~value);
}

int stdc_trailing_onesull(unsigned long long value)
{
    return stdc_trailing_zerosull(~value);
}

