#include <stdbit.h>

/*
 * TODO: Verify the expected behaviour
 */

int stdc_first_leading_oneuc(unsigned char value)
{
    if (value == 0)
        return 0;
    return stdc_leading_zerosuc(value) + 1;
}

int stdc_first_leading_oneus(unsigned short value)
{
    if (value == 0)
        return 0;
    return stdc_leading_zerosus(value) + 1;
}

int stdc_first_leading_oneui(unsigned int value)
{
    if (value == 0u)
        return 0;
    return stdc_leading_zerosui(value) + 1;
}

int stdc_first_leading_oneul(unsigned long value)
{
    if (value == 0ul)
        return 0;
    return stdc_leading_zerosul(value) + 1;
}

int stdc_first_leading_oneull(unsigned long long value)
{
    if (value == 0ull)
        return 0;
    return stdc_leading_zerosull(value) + 1;
}
