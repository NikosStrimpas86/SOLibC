#include <math.h>
#if __solc_math_errhandling & __SOLC_MATH_ERREXCEPT
#include <fenv.h>
#endif


int __iseqsig(double x, double y)
{
    /* FIXME: I don't like this */
#if __solc_math_errhandling & __SOLC_MATH_ERREXCEPT
    if (__solc_fpclassify(x) == 2 || __solc_fpclassify(y) == 2)
        feraiseexcept(FE_INVALID);
#endif
    return x == y;
}

int __iseqsigf(float x, float y)
{
    /* FIXME: I don't like this */
#if __solc_math_errhandling & __SOLC_MATH_ERREXCEPT
    if (__solc_fpclassify(x) == 2 || __solc_fpclassify(y) == 2)
        feraiseexcept(FE_INVALID);
#endif
    return x == y;
}

int __iseqsigl(long double x, long double y)
{
    /* FIXME: I don't like this */
#if __solc_math_errhandling & __SOLC_MATH_ERREXCEPT
    if (__solc_fpclassify(x) == 2 || __solc_fpclassify(y) == 2)
        feraiseexcept(FE_INVALID);
#endif
    return x == y;
}
