#include <math.h>
#include <math_types.h>

double copysign(double x, double y)
{
    union _Double_extract capture_x = { .number = x };
    union _Double_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffffffffffff;
    capture_x.representation |= (capture_y.representation & 0x8000000000000000);

    return capture_x.number;
}

float copysignf(float x, float y)
{
    union _Float_extract capture_x = { .number = x };
    union _Float_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffff;
    capture_x.representation |= (capture_y.representation & 0x80000000);

    return capture_x.number;
}

long double copysignl(long double x, long double y)
{
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    union _Ldouble_extract capture_x = { .number = x };
    union _Ldouble_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffffffffffff;
    capture_x.representation |= (capture_y.representation & 0x8000000000000000);

    return capture_x.number;

#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
#    if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)
    _Static_assert(sizeof(long double) >= 10, "There's a problem with long double");
    _Static_assert(sizeof(long double) <= 16, "There's a problem with long double");
#    endif
    union _Ldouble_extract capture_x = { .number = x };
    union _Ldouble_extract capture_y = { .number = y };
    capture_x.representation.sign_and_exponent &= 0x7fff;
    capture_x.representation.sign_and_exponent |= (capture_y.representation.sign_and_exponent & 0x8000);

    return capture_x.number;

#else

    if ((x < 0 && y > 0) || (x > 0 && y < 0))
        return -x;
    return x;
#endif
}

#ifdef __STDC_IEC_60559_DFP__

/* TODO: Add implementations for _DecimalNx types */

_Decimal32 copysignd32(_Decimal32 x, _Decimal32 y)
{
    union _Decimal32_extract capture_x = { .number = x };
    union _Decimal32_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffff;
    capture_x.representation |= (capture_y.representation & 0x80000000);

    return capture_x.number;
}

_Decimal64 copysignd64(_Decimal64 x, _Decimal64 y)
{
    union _Decimal64_extract capture_x = { .number = x };
    union _Decimal64_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffffffffffff;
    capture_x.representation |= (capture_y.representation & 0x8000000000000000);

    return capture_x.number;
}

_Decimal128 copysignd128(_Decimal128 x, _Decimal128 y)
{
    _Decimal128_extract capture_x = { .number = x };
    _Decimal128_extract capture_y = { .number = y };
    capture_x.representation[1] &= 0x7fffffffffffffff;
    capture_x.representation[1] |= (capture_y.representation[1] & 0x8000000000000000);

    return capture_x.number;
}

#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_BFP__

/* TODO: Add implementations for _FloatNx types */

_Float16 copysignf16(_Float16 x, _Float16 y)
{
    union _Float16_extract capture_x = { .number = x };
    union _Float16_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fff;
    capture_x.representation |= (capture_y.represenation & 0x8000);

    return capture_x.number;
}

_Float32 copysignf32(_Float32 x, _Float32 y)
{
    union _Float32_extract capture_x = { .number = x };
    union _Float64_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffff;
    capture_x.representation |= (capture_y.representation & 0x80000000);

    return capture_x.number;
}

_Float64 copysignf64(_Float64 x, _Float64 y)
{
    union _Float64_extract capture_x = { .number = x };
    union _Float64_extract capture_y = { .number = y };
    capture_x.representation &= 0x7fffffffffffffff;
    capture_x.representation |= (capture_y.representation & 0x8000000000000000);

    return capture_x.number;
}

_Float128 copysignf128(_Float128 x, _Float128 y)
{
    union _Float128_extract capture_x = { .number = x };
    union _Float128_extract capture_y = { .number = y };
    capture_x.representation[1] &= 0x7fffffffffffffff;
    capture_x.representation[1] = (capture_y.representation[1] & 0x8000000000000000);

    return capture_y.number;
}

#endif /* __STDC_IEC_60559_BFP__ */
