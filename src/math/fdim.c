#include <math.h>

/* FIXME: Handle overflow */
double fdim(double x, double y)
{
    if (isnan(x))
        return x;
    if (isnan(y))
        return y;

    return x > y ? x - y : 0.0;
}

/* FIXME: Handle overflow */
float fdimf(float x, float y)
{
    if (isnan(x))
        return x;
    if (isnan(y))
        return y;

    return x > y ? x - y : 0.0f;
}

/* FIXME: Handle overflow */
long double fdiml(long double x, long double y)
{
    if (isnan(x))
        return x;
    if (isnan(y))
        return y;

    return x > y ? x - y : 0.0L;
}

#ifdef __STDC_IEC_60559_DFP__

/* FIXME: Handle overflow */
_Decimal32 fdimd32(_Decimal32 x, _Decimal32 y)
{
    if (isnan(x))
        return x;
    if (isnan(y))
        return y;

    return x > y ? x - y : 0.0;
}

/* FIXME: Handle overflow */
_Decimal64 fdimd64(_Decimal64 x, _Decimal64 y)
{
    if (isnan(x))
        return x;
    if (isnan(y))
        return y;

    return x > y ? x - y : 0.0;
}

/* FIXME: Handle overflow */
_Decimal128 fdimd128(_Decimal128 x, _Decimal128 y)
{
    if (isnan(x))
        return x;
    if (isnan(y))
        return y;

    return x > y ? x - y : 0.0;
}

#endif /* __STDC_IEC_60559_DFP__ */
