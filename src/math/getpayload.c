#include <math.h>
#include <math_types.h>

#ifdef __STDC_IEC_60559_BFP__

float getpayloadf(const float* x)
{
    static_assert(sizeof(float) * CHAR_BIT == 32, "Format of type float is not right");
    union _Float_extract captured = { .number = *x };
    if ((captured.representation & UINT32_C(0x7f800000)) != UINT32_C(0x7f800000)
        || (captured.representation & UINT32_C(0x7fffff)) == 0)
        return -1.0f;
    captured.representation &= UINT32_C(0x3fffff);
    return (float)captured.representation;
}

double getpayload(const double* x)
{
    static_assert(sizeof(double) * CHAR_BIT == 64, "Format of type float is not right");
    union _Double_extract captured = { .number = *x };
    if ((captured.representation & UINT64_C(0x7ff0000000000000)) != UINT64_C(0x7ff0000000000000)
        || (captured.representation & UINT64_C(0xfffffffffffff)) == 0)
        return -1.0;
    captured.representation &= UINT64_C(0x7ffffffffffff);
    return (double)captured.representation;
}

#endif
