#include <wchar.h>

wchar_t* wmemcpy(wchar_t* restrict s1, const wchar_t* restrict s2, size_t n)
{
    if ((s1 == NULL) || (s2 == NULL)) {
        return NULL;
    }
    if (s1 == s2) {
        return s1;
    }

    for (size_t i = 0; i < n; i++) {
        s1[i] = s2[i];
    }
    return s1;
}
