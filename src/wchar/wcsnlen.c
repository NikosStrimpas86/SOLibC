#include <wchar.h>


size_t wcsnlen(const wchar_t* ws, size_t maxlen)
{
    wchar_t const* iterator = ws;
    for (; (maxlen--) && (*iterator != L'\0'); iterator++);

    return iterator - ws;
}
