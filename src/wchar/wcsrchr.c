#include <wchar.h>

wchar_t* (wcsrchr)(const wchar_t* s, wchar_t c)
{
    size_t length = wcslen(s);
    for (size_t i = 0; i <= length; i++) {
        if (s[length - i] == (wchar_t const)c) {
            return (wchar_t*)(&s[length - i]);
        }
    }
    return NULL;
}
