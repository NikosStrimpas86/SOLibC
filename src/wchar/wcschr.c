#include <wchar.h>

wchar_t* (wcschr)(const wchar_t* s, wchar_t c)
{
    size_t length = wcslen(s);
    for (size_t i = 0; i <= length; i++) {
        if (s[i] == c) {
            return (wchar_t*)(&s[i]);
        }
    }
    return NULL;
}
