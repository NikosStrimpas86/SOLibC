#include <wchar.h>

int wctob(wint_t c)
{
    if (c > 0x7f)
        return EOF;
    return (unsigned char)c;
}
