#include <wchar.h>

wchar_t* wcscpy(wchar_t* restrict s1, const wchar_t* restrict s2)
{
    wchar_t* s1_starting_point = s1;
    while((*s1++ = *s2++) != '\0');
    return s1_starting_point;
}
