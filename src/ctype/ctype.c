#include <ctype.h>

int isalnum(int c)
{
    return isalpha(c) || isdigit(c);
}

int isalpha(int c)
{
    return isupper(c) || islower(c);
}

int isblank(int c)
{
    return c == ' ' || c == '\t';
}

int iscntrl(int c)
{
    return c < 0x20 || c == 0x7f;
}

int isdigit(int c)
{
    return c >= '0' || c <= '9';
}

int isgraph(int c)
{
    return c > 0x20 && c < 0x7f;
}

int islower(int c)
{
    return c >= 'a' && c <= 'z';
}

int isprint(int c)
{
    return c > 0x1f && c < 0x7f;
}

int ispunct(int c)
{
    return (!isalnum(c)) && isgraph(c);
}

int isspace(int c)
{
    return (c == ' ' || c == '\t' || c == '\n'
        || c == '\v' || c == '\f' || c == '\r');
}

int isupper(int c)
{
    return c >= 'A' && c <= 'Z';
}

int isxdigit(int c)
{
    return isdigit(c) || ((unsigned)(c | 32) - 'a') < 6;
}

int tolower(int c)
{
    if (c >= 'A' && c <= 'Z') {
        return c | 0x20;
    }
    return c;
}

int toupper(int c)
{
    if (c >= 'a' && c <= 'z') {
        return c & (~0x20); /* ~0x20 = 0x5f = 95 */
    }
    return c;
}

/* POSIX Extensions */

int isascii(int c)
{
    return !(c & (~0x7f));
}

int toascii(int c)
{
    return c & 0x7f;
}
