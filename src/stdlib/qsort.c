#include <limits.h>
#include <stdint.h>
#include <stdlib.h>

/*
 * Testing results on this implementation showed that it doesn't perform really well on already sorted arrays,
 * nor on small arrays. Where it really shines is on the worst case scenario with huge arrays either filled with
 * random values, or filled with values complementary to the sorting condition.
 */

#if (SIZE_WIDTH == ULLONG_WIDTH)
typedef long long int __signed_iterator;
#    define __SIGNED_ITERATOR_MAX LLONG_MAX
#elif (SIZE_WIDTH == ULONG_WIDTH)
typedef long int __signed_iterator;
#    define __SIGNED_ITERATOR_MAX LONG_MAX
#else
typedef long long int __signed_iterator;
#    define __SIGNED_ITERATOR_MAX LLONG_MAX
#endif

/* Both swap and quicksort are defined as static functions because no one should be able to call them in another file */

/*
 * I wanted to use a SWAP macro, but it either causes segmentation fault, or it just doesn't work
 * So this function is a temporary solution
 *
 * Update: After some testing in godbolt, I found out that the algorithm is faster if swap is a macro.
 *         GCC likes it more if swap is inline and pretty much makes the function almost 2 times faster.
 *         Clang likes it more if swap isn't inline. I suspect that this is because of some cache trick.
 *         Cproc doesn't mind.
 */
inline static void swap(void* restrict base, size_t size, size_t a, size_t b)
{
    unsigned char* restrict f = ((unsigned char*)base) + a * size;
    unsigned char* restrict l = ((unsigned char*)base) + b * size;
    while (size) {
        unsigned char temp = *f;
        *f = *l;
        *l = temp;
        --size;
        ++f;
        ++l;
    }
}

/*
 * TODO (maybe): This could be improved using tail call optimization.
 *               I don't know whether the explicit stack would be a good choice.
 */
static void quicksort(unsigned char* base, size_t size, __signed_iterator left, __signed_iterator right, int (*compar)(const void*, const void*))
{
    __signed_iterator iterator = left - 1, reverse_iterator = right, pseudo_pivot = left - 1, reverse_pseudo_pivot = right, index_counter;
    /* Making this pointer restrict makes the algorithm really fast, but I want to test it more. */
    unsigned char* restrict pivot_point = base + right * size;

    /* Sanity check */
    if (right <= left) {
        return;
    }

    /* Swap the middle element with the last one to define the real pivot */
    swap(base, size, left + (right - left) / 2, right);
    /* Partitioning section */
    for (;;) {
        /* Normal iteration through the array, using three-way comparison */
        while (++iterator != right && compar(base + iterator * size, pivot_point) < 0)
            ;
        /* Reverse iteration through the array, using three-way comparison */
        while (compar(pivot_point, base + (--reverse_iterator) * size) < 0) {
            /* The paper insists that this should be here */
            if (reverse_iterator == left) {
                break;
            }
        }

        /* Nothing to partition really */
        if (iterator >= reverse_iterator) {
            break;
        }
        /* First swap that is almost always used in quicksort */
        swap(base, size, iterator, reverse_iterator);
        /* And here is the usual quicksort partitioning routine */
        if (compar(base + iterator * size, pivot_point) == 0) {
            swap(base, size, ++pseudo_pivot, iterator);
        }
        if (compar(pivot_point, base + reverse_iterator * size) == 0) {
            swap(base, size, reverse_iterator, --reverse_pseudo_pivot);
        }
    }
    /* Last swap that is always used in quicksort */
    swap(base, size, iterator, right);
    reverse_iterator = iterator - 1;
    ++iterator;

    /* Pretty much these two loops are used for good measure. Most arrays are sorted fine without them. */
    for (index_counter = left; index_counter < pseudo_pivot; index_counter++, reverse_iterator--) {
        swap(base, size, index_counter, reverse_iterator);
    }
    for (index_counter = right - 1; index_counter > reverse_pseudo_pivot; index_counter--, iterator++) {
        swap(base, size, iterator, index_counter);
    }

    /* Recursive calls */
    quicksort(base, size, left, reverse_iterator, compar);
    quicksort(base, size, iterator, right, compar);
}

/*
 * This implementation of quick sort is based on the 3-way partitioning quicksort, that was recommended to me in an online forum,
 * after the failure of a simple quicksort algorithm to sort strings, without it becoming a mess.
 * I was also recommended to define a signed type with width equal to that of size_t for the indices, because -1 was a valid value.
 * The paper that was given to me is the following:
 * https://www.cs.princeton.edu/~rs/talks/QuicksortIsOptimal.pdf
 */
void qsort(void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*))
{
    if (nmemb == 0 || size == 0) {
        return;
    }

    /*
     * TODO: Use a different algorithm for smaller arrays.
     *       Tests showed that the implementation takes a little too long to sort small arrays.
     *       Should probably look at something like insertion-sort for this, though I know that
     *       some implementations use (or used) mergesort.
     */
    if (nmemb > 1) {
        /*
         * NOTE: In case of bugs, check this. Revert this back to left = 0 and right = nmemb - 1 if needed.
         */
        unsigned char* new_base = ((unsigned char*)base) + size * (nmemb / 2);
        __signed_iterator left = -((__signed_iterator)(nmemb / 2));
        __signed_iterator right;
        if (nmemb & (size_t)0x1) {
            right = ((__signed_iterator)(nmemb / 2));
        } else {
            right = ((__signed_iterator)(nmemb / 2)) - (__signed_iterator)1;
        }
        quicksort(new_base, size, left, right, compar);
    }
}
