#include <string.h>

void* memset(void* s, int c, size_t n)
{
    unsigned char* deref_s = (unsigned char*) s;
    for (size_t i = 0; i < n; i++) {
        deref_s[i] = (unsigned char) c;
    }
    return s;
}
