#include <string.h>

int memcmp(const void* s1, const void* s2, size_t n)
{
    const unsigned char* s1_object_chunk = (const unsigned char*) s1;
    const unsigned char* s2_object_chunk = (const unsigned char*) s2;

    for (size_t i = 0; i < n; i++) {
        if (s1_object_chunk[i] != s2_object_chunk[i]) {
            return s1_object_chunk[i] < s2_object_chunk[i] ? -1 : 1;
        }
    }
    return 0;
}
