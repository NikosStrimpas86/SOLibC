#include <complex.h>
#include <complex_types.h>

double complex conj(double complex z)
{
    union _Double_complex_parts captured = { .number = z };
    captured.as_array[1] = -captured.as_array[1];
    return captured.number;
}

float complex conjf(float complex z)
{
    union _Float_complex_parts captured = { .number = z };
    captured.as_array[1] = -captured.as_array[1];
    return captured.number;
}

long double complex conjl(long double complex z)
{
    union _Ldouble_complex_parts captured = { .number = z };
    captured.as_array[1] = -captured.as_array[1];
    return captured.number;
}
